﻿using GalaSoft.MvvmLight.Messaging;
using ScrollingAnimationsMVVMLight.ViewModels;

namespace ScrollingAnimationsMVVMLight.Tests
{
    public class PanelScrollerControlBuilder
    {
        private IMessenger mMessenger;
        private bool mSetAllScrollFlags = false;

        public static PanelScrollerControlBuilder Create()
        {
            return new PanelScrollerControlBuilder();
        }

        public PanelScrollerControlBuilder WithMessenger(IMessenger messenger)
        {
            mMessenger = messenger;
            return this;
        }

        public PanelScrollerControlBuilder WithAllScrollFlagsSet()
        {
            mSetAllScrollFlags = true;
            return this;
        }

        public PanelScrollerControl Build()
        {
            return this;
        }

        public static implicit operator PanelScrollerControl(PanelScrollerControlBuilder builder)
        {
            var control = new PanelScrollerControl(builder.mMessenger);

            if (builder.mSetAllScrollFlags)
            {
                control.ScrollLeft = true;
                control.ScrollLeftWrap = true;
                control.ScrollRight = true;
                control.ScrollRightWrap = true;
            }

            return control;
        }
    }
}
