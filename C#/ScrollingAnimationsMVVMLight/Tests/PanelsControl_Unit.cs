﻿using System.Collections.Generic;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using ScrollingAnimationsMVVMLight.Utiltilities;
using Xunit;

namespace ScrollingAnimationsMVVMLight.Tests
{
    public class PanelsControl_Unit
    {
        [Fact]
        [Trait("Category", "Unit")]
        void Ctr_WithTwoImages_SetsOnScreenImageToFirstAndOffScreenImageToSecondImage()
        {
            var messenger = TestMessengerBuilder.Create().Build();
            var panels = CreatePanels("image1", "image2");
            var testSystemWrapper = new TestSystemWrapper();

            var control = PanelsControlBuilder.Create().WithMessenger(messenger).WithPanels(panels).WithSystemWrapper(testSystemWrapper).Build();

            Assert.Equal<ImageSource>(control.OnScreenImageSource, testSystemWrapper.Images["image1"] as ImageSource);
            Assert.Equal<ImageSource>(control.OffScreenImageSource, testSystemWrapper.Images["image2"] as ImageSource);
        }

        [Fact]
        [Trait("Category", "Unit")]
        void ScrollRightEvent_Raised_UpdatesBothImageSources()
        {
            var messenger = TestMessengerBuilder.Create().Build();
            var panels = CreatePanels("image1", "image2");
            var testSystemWrapper = new TestSystemWrapper();
            var control = PanelsControlBuilder.Create().WithMessenger(messenger).WithPanels(panels).WithSystemWrapper(testSystemWrapper).Build();

            messenger.Send<ScrollEvent>(new ScrollEvent(Scroll.Direction.Right));

            Assert.Equal<ImageSource>(control.OnScreenImageSource, testSystemWrapper.Images["image2"] as ImageSource);
            Assert.Equal<ImageSource>(control.OffScreenImageSource, testSystemWrapper.Images["image1"] as ImageSource);
        }

        [Fact]
        [Trait("Category", "Unit")]
        void ScrollLeftEvent_RaisedAndFarLeftPanelSelected_WrapsAroundAndUpdatesBothImageSources()
        {
            var messenger = TestMessengerBuilder.Create().Build();
            var panels = CreatePanels("image1", "image2", "image3");
            var testSystemWrapper = new TestSystemWrapper();
            var control = PanelsControlBuilder.Create().WithMessenger(messenger).WithPanels(panels).WithSystemWrapper(testSystemWrapper).Build();

            messenger.Send<ScrollEvent>(new ScrollEvent(Scroll.Direction.Left));

            Assert.Equal<ImageSource>(control.OnScreenImageSource, testSystemWrapper.Images["image3"] as ImageSource);
            Assert.Equal<ImageSource>(control.OffScreenImageSource, testSystemWrapper.Images["image1"] as ImageSource);
        }

        [Fact]
        [Trait("Category", "Unit")]
        void MultipleScrollRightEvents_RaisedBeforeFirstScrollCompletes_IgnoresScrollEventsWhileStillScrolling()
        {
            var messenger = TestMessengerBuilder.Create().Build();
            var panels = CreatePanels("image1", "image2", "image3");
            var testSystemWrapper = new TestSystemWrapper();
            var control = PanelsControlBuilder.Create().WithMessenger(messenger).WithPanels(panels).WithSystemWrapper(testSystemWrapper).Build();

            messenger.Send<ScrollEvent>(new ScrollEvent(Scroll.Direction.Right));
            messenger.Send<ScrollEvent>(new ScrollEvent(Scroll.Direction.Right));
            messenger.Send<ScrollEvent>(new ScrollEvent(Scroll.Direction.Right));

            Assert.Equal<ImageSource>(control.OnScreenImageSource, testSystemWrapper.Images["image2"] as ImageSource);
            Assert.Equal<ImageSource>(control.OffScreenImageSource, testSystemWrapper.Images["image1"] as ImageSource);
        }

        [Fact]
        [Trait("Category", "Unit")]
        void MultipleScrollLeftEvents_RaisedAfterEachScrollCompletes_ScrollsToExpectedPanel()
        {
            var messenger = TestMessengerBuilder.Create().Build();
            var panels = CreatePanels("image1", "image2", "image3", "image4", "image5");
            var testSystemWrapper = new TestSystemWrapper();
            var control = PanelsControlBuilder.Create().WithMessenger(messenger).WithPanels(panels).WithSystemWrapper(testSystemWrapper).Build();

            messenger.Send<ScrollEvent>(new ScrollEvent(Scroll.Direction.Left));
            control.FinishScrollingCommand.Execute(this);
            messenger.Send<ScrollEvent>(new ScrollEvent(Scroll.Direction.Left));
            control.FinishScrollingCommand.Execute(this);
            messenger.Send<ScrollEvent>(new ScrollEvent(Scroll.Direction.Left));
            control.FinishScrollingCommand.Execute(this);

            Assert.Equal<ImageSource>(control.OnScreenImageSource, testSystemWrapper.Images["image3"] as ImageSource);
            Assert.Equal<ImageSource>(control.OffScreenImageSource, testSystemWrapper.Images["image4"] as ImageSource);
        }

        [Fact]
        [Trait("Category", "Unit")]
        void FinishScrollingCommand_InvokedAfterAScroll_RaisesFinishScrolllingEvent()
        {
            var finishScrollingEventRaised = false;
            var messenger = TestMessengerBuilder.Create().WithFinishScrollingEventHandler((f) => { finishScrollingEventRaised = true; }).Build();
            var panels = CreatePanels("image1", "image2");
            var testSystemWrapper = new TestSystemWrapper();
            var control = PanelsControlBuilder.Create().WithMessenger(messenger).WithPanels(panels).WithSystemWrapper(testSystemWrapper).Build();

            messenger.Send<ScrollEvent>(new ScrollEvent(Scroll.Direction.Right));
            control.FinishScrollingCommand.Execute(this);

            Assert.True(finishScrollingEventRaised);
        }

        private List<PanelsPanel> CreatePanels(params string[] filenames)
        {
            var panels = new List<PanelsPanel>();

            foreach (var filename in filenames)
            {
                panels.Add(new PanelsPanel() { Background = filename });
            }
            return panels;
        }

        private class TestSystemWrapper : SystemWrapperBase
        {
            public Dictionary<string, BitmapImage> Images = new Dictionary<string, BitmapImage>();

            public override BitmapImage CreateBitmapImage(string filename)
            {
                Images[filename] = new BitmapImage();
                return Images[filename];
            }
        }
    }
}
