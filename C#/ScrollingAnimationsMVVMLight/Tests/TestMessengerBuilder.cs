﻿using System;
using GalaSoft.MvvmLight.Messaging;
using ScrollingAnimationsMVVMLight.Utiltilities;

namespace ScrollingAnimationsMVVMLight.Tests
{
    public class TestMessengerBuilder
    {
        IMessenger mMessenger;
        Action<FinishScrollingEvent> mFinishScrollingEventHandler;
        Action<ScrollEvent> mScrollEventHandler;

        public static TestMessengerBuilder Create()
        {
            return new TestMessengerBuilder();
        }

        public TestMessengerBuilder WithFinishScrollingEventHandler(Action<FinishScrollingEvent> finishScrollingEventHandler)
        {
            mFinishScrollingEventHandler = finishScrollingEventHandler;
            return this;
        }

        public TestMessengerBuilder WithScrollEventHandler(Action<ScrollEvent> scrollEventHandler)
        {
            mScrollEventHandler = scrollEventHandler;
            return this;
        }

        public TestMessengerBuilder WithMessenger(IMessenger messenger)
        {
            mMessenger = messenger;
            return this;
        }

        public IMessenger Build()
        {
            var messenger =  new Messenger();

            if(mFinishScrollingEventHandler != null)
            {
                messenger.Register<FinishScrollingEvent>(messenger, mFinishScrollingEventHandler);
            }

            if(mScrollEventHandler != null)
            {
                messenger.Register<ScrollEvent>(messenger, mScrollEventHandler);
            }

            return messenger;
        }
    }
}
