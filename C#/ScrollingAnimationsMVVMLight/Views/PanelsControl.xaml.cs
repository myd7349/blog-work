﻿using System.Windows;
using System.Windows.Controls;

namespace ScrollingAnimationsMVVMLight.Views
{
    /// <summary>
    /// Interaction logic for PanelsControl.xaml
    /// </summary>
    public partial class PanelsControl : UserControl
    {
        public static readonly DependencyProperty ScrollWidthProperty = DependencyProperty.Register("ScrollWidth", typeof(float), typeof(PanelScrollerControl));
        public float ScrollWidth
        {
            get { return (float)GetValue(ScrollWidthProperty); }
            set
            {
                SetValue(ScrollPanelLeftWidthProperty, -value);
                SetValue(ScrollPanelRightWidthProperty, value);
            }
        }

        public static readonly DependencyProperty ScrollPanelLeftWidthProperty = DependencyProperty.Register("ScrollPanelLeftWidth", typeof(float), typeof(PanelScrollerControl));
        public float ScrollPanelLeftWidth
        {
            get { return (float)GetValue(ScrollPanelLeftWidthProperty); }
        }

        public static readonly DependencyProperty ScrollPanelRightWidthProperty = DependencyProperty.Register("ScrollPanelRightWidth", typeof(float), typeof(PanelScrollerControl));
        public float ScrollPanelRightWidth
        {
            get { return (float)GetValue(ScrollPanelRightWidthProperty); }
        }

        public PanelsControl()
        {
            InitializeComponent();
        }
    }
}
