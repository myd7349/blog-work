﻿using Prism.Events;
using ScrollingAnimationsPrism.ViewModels;

namespace ScrollingAnimationsPrism.Tests
{
    public class PanelScrollerControlViewModelBuilder
    {
        private IEventAggregator mEventAggregator = new EventAggregator();
        private bool mSetAllScrollFlags = false;

        public static PanelScrollerControlViewModelBuilder Create()
        {
            return new PanelScrollerControlViewModelBuilder();
        }

        public PanelScrollerControlViewModelBuilder WithEventAggregator(IEventAggregator eventAggregator)
        {
            mEventAggregator = eventAggregator;
            return this;
        }

        public PanelScrollerControlViewModelBuilder WithAllScrollFlagsSet()
        {
            mSetAllScrollFlags = true;
            return this;
        }

        public PanelScrollerControlViewModel Build()
        {
            return this;
        }

        public static implicit operator PanelScrollerControlViewModel(PanelScrollerControlViewModelBuilder builder)
        {
            var control = new PanelScrollerControlViewModel(builder.mEventAggregator);

            // Ensure that the property changed event handler is hooked up
            control.PropertyChanged += (o, e) => {};

            if (builder.mSetAllScrollFlags)
            {
                control.ScrollLeft = true;
                control.ScrollLeftWrap = true;
                control.ScrollRight = true;
                control.ScrollRightWrap = true;
            }

            return control;
        }
    }
}
