﻿using System;
using ScrollingAnimationsPrism.Utiltilities;
using Xunit;

namespace ScrollingAnimationsPrism.Tests
{
    public class PanelScrollerControlViewModel_Unit
    {
        [Fact]
        [Trait("Category", "Unit")]
        void Ctr_NullEventAggregator_ThrowsArgumentNullException()
        {
            Action constructAction = () => PanelScrollerControlViewModelBuilder.Create().WithEventAggregator(null).Build();

            Assert.Throws<ArgumentNullException>("eventAggregator", constructAction);
        }

        [Fact]
        [Trait("Category", "Unit")]
        void NumPanels_ZeroPanelCount_ThrowsArgumentException()
        {
            var control = PanelScrollerControlViewModelBuilder.Create().Build();
            Action setNumPanelsAction = () => control.NumPanels = 0;

            Assert.Throws<ArgumentException>("NumPanels", setNumPanelsAction);
        }

        [Fact]
        [Trait("Category", "Unit")]
        void ScrollLeftCommand_Invoked_PublishesScrollEvent()
        {
            var scrolledLeft = false;
            var eventAggregator = EventAggregatorBuilder.Create().WithScrollEventHandler((d) => { scrolledLeft = (d == Scroll.Direction.Left); }).Build();
            var control = PanelScrollerControlViewModelBuilder.Create().WithEventAggregator(eventAggregator).Build();

            control.ScrollLeftCommand.Execute(this);

            Assert.True(scrolledLeft);
        }

        [Fact]
        [Trait("Category", "Unit")]
        void MultipleScrollLeftCommands_InvokedBeforeFirstScrollCompletes_IgnoresScrollCommandsWhileStillScrolling()
        {
            var eventRaisedCount = 0;
            var eventAggregator = EventAggregatorBuilder.Create().WithScrollEventHandler((d) => { ++eventRaisedCount; }).Build();
            var control = PanelScrollerControlViewModelBuilder.Create().WithEventAggregator(eventAggregator).Build();

            control.ScrollLeftCommand.Execute(this);
            control.ScrollLeftCommand.Execute(this);
            control.ScrollLeftCommand.Execute(this);

            Assert.Equal(1, eventRaisedCount);
        }

        [Fact]
        [Trait("Category", "Unit")]
        void MultipleScrollRightCommands_InvokedAfterEachPreviousScrollCompletes_RaisesScrollEventEachTime()
        {
            var eventRaisedCount = 0;
            var eventAggregator = EventAggregatorBuilder.Create().WithScrollEventHandler((d) => { if(d == Scroll.Direction.Right) ++eventRaisedCount; }).Build();
            var control = PanelScrollerControlViewModelBuilder.Create().WithEventAggregator(eventAggregator).Build();

            control.ScrollLeftCommand.Execute(this);
            eventAggregator.GetEvent<ScrollEvent>().Publish(Scroll.Direction.Right);
            control.ScrollLeftCommand.Execute(this);
            eventAggregator.GetEvent<ScrollEvent>().Publish(Scroll.Direction.Right);
            control.ScrollLeftCommand.Execute(this);
            eventAggregator.GetEvent<ScrollEvent>().Publish(Scroll.Direction.Right);

            Assert.Equal(3, eventRaisedCount);
        }

        [Fact]
        [Trait("Category", "Unit")]
        void ScrollRightCommand_Invoked_RaisesScrollEvent()
        {
            var scrolledRight = false;
            var eventAggregator = EventAggregatorBuilder.Create().WithScrollEventHandler((d) => { scrolledRight = (d == Scroll.Direction.Right); }).Build();
            var control = PanelScrollerControlViewModelBuilder.Create().WithEventAggregator(eventAggregator).Build();

            control.ScrollRightCommand.Execute(this);

            Assert.True(scrolledRight);
        }

        [Fact]
        [Trait("Category", "Unit")]
        void FinishScrollingHandler_InvokedAfterStartingAScroll_ClearsScrollFlags()
        {
            var eventAggregator = EventAggregatorBuilder.Create().Build();
            var control = PanelScrollerControlViewModelBuilder.Create().WithEventAggregator(eventAggregator).WithAllScrollFlagsSet().Build();

            control.ScrollRightCommand.Execute(this);
            eventAggregator.GetEvent<FinishScrollingEvent>().Publish(null);

            Assert.False(control.ScrollLeft);
            Assert.False(control.ScrollLeftWrap);
            Assert.False(control.ScrollRight);
            Assert.False(control.ScrollRightWrap);
        }
    }
}
