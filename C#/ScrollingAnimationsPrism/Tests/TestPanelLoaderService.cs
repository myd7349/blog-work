﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ScrollingAnimationsPrism.Services;

namespace ScrollingAnimationsPrism.Tests
{
    public class TestPanelLoaderService : IPanelLoaderService
    {
        private IEnumerable<PanelsPanel> mPanels;

        public TestPanelLoaderService(IEnumerable<PanelsPanel> panels)
        {
            mPanels = panels;
        }

        // We want this method to run synchronously for the tests
        // so disable the warning about not having any await
#pragma warning disable 1998
        public async Task<IEnumerable<PanelsPanel>> LoadPanels()
        {
            return Task.Run(() => {return mPanels;}).Result;
        }
#pragma warning restore 1998
    }
}
