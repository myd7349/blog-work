﻿using System;
using Prism.Events;
using ScrollingAnimationsPrism.Utiltilities;

namespace ScrollingAnimationsPrism.Tests
{
    public class EventAggregatorBuilder
    {
        Action<object> mFinishScrollingEventHandler;
        Action<Scroll.Direction> mScrollEventHandler;

        public static EventAggregatorBuilder Create()
        {
            return new EventAggregatorBuilder();
        }

        public EventAggregatorBuilder WithFinishScrollingEventHandler(Action<object> finishScrollingEventHandler)
        {
            mFinishScrollingEventHandler = finishScrollingEventHandler;
            return this;
        }

        public EventAggregatorBuilder WithScrollEventHandler(Action<Scroll.Direction> scrollEventHandler)
        {
            mScrollEventHandler = scrollEventHandler;
            return this;
        }

        public EventAggregator Build()
        {
            var eventAggregator = new EventAggregator();

            if (mFinishScrollingEventHandler != null)
            {
                eventAggregator.GetEvent<FinishScrollingEvent>().Subscribe(mFinishScrollingEventHandler);
            }

            if (mScrollEventHandler != null)
            {
                eventAggregator.GetEvent<ScrollEvent>().Subscribe(mScrollEventHandler);
            }

            return eventAggregator;
        }
    }
}
