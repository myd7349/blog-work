﻿using System.Collections.Generic;
using Prism.Events;
using ScrollingAnimationsPrism.Services;
using ScrollingAnimationsPrism.Utiltilities;
using ScrollingAnimationsPrism.ViewModels;

namespace ScrollingAnimationsPrism.Tests
{
    public class PanelsControlViewModelBuilder
    {
        private IEventAggregator mEventAggregator = new EventAggregator();
        private SystemWrapperBase mSystemWrapper = new SystemWrapper();
        private IPanelLoaderService mPanelLoaderService = null;

        public PanelsControlViewModelBuilder WithEventAggregator(IEventAggregator eventAggregator)
        {
            mEventAggregator = eventAggregator;
            return this;
        }

        public PanelsControlViewModelBuilder WithPanels(List<PanelsPanel> panels)
        {
            mPanelLoaderService = new TestPanelLoaderService(panels);
            return this;
        }

        public PanelsControlViewModelBuilder WithPanelLoaderService(IPanelLoaderService service)
        {
            mPanelLoaderService = service;
            return this;
        }

        public PanelsControlViewModelBuilder WithSystemWrapper(SystemWrapperBase systemWrapper)
        {
            mSystemWrapper = systemWrapper;
            return this;
        }

        public PanelsControlViewModel Build()
        {
            return this;
        }

        public static implicit operator PanelsControlViewModel(PanelsControlViewModelBuilder builder)
        {
            var control = new PanelsControlViewModel(
                builder.mSystemWrapper,
                builder.mPanelLoaderService,
                builder.mEventAggregator);

            // Ensure that the property changed event handler is hooked up
            control.PropertyChanged += (o, e) => { };

            control.HookupImagesToPanels();

            return control;
        }
    }
}
