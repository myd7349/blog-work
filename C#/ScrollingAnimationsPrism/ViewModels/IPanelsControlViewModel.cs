﻿using System.Windows.Input;
using System.Windows.Media;
using ScrollingAnimationsPrism.Utiltilities;

namespace ScrollingAnimationsPrism.ViewModels
{
    interface IPanelsControlViewModel
    {
        ImageSource OnScreenImageSource { get; set; }
        ImageSource OffScreenImageSource { get; set; }
        ICommand FinishScrollingCommand { get; }
        bool ScrollLeft { get; set; }
        bool ScrollRight { get; set; }

        void HookupImagesToPanels();
        void ScrollHandler(Scroll.Direction direction);
    }
}
