﻿using System;
using Prism.Events;

namespace ScrollingAnimationsPrism.Utiltilities
{
    public class Scroll
    {
        public enum Direction { Left, Right };
    }

    public class ScrollEvent : PubSubEvent<Scroll.Direction>{}
    public class FinishScrollingEvent : PubSubEvent<object> { }
    public class PanelsLoadedEvent : PubSubEvent<uint> { }
}
