﻿using System.Windows;
using Microsoft.Practices.Unity;
using Prism.Unity;
using ScrollingAnimationsPrism.Services;
using ScrollingAnimationsPrism.ViewModels;
using ScrollingAnimationsPrism.Views;

namespace ScrollingAnimationsPrism
{
    public class Bootstrapper : UnityBootstrapper
    {
        protected override DependencyObject CreateShell()
        {
            return Container.Resolve<MainWindow>();
        }

        protected override void InitializeShell()
        {
            Application.Current.MainWindow.Show();
        }

        protected override void ConfigureViewModelLocator()
        {
            base.ConfigureViewModelLocator();

        }

        protected override void ConfigureContainer()
        {
            base.ConfigureContainer();

            RegisterTypeIfMissing(typeof(IPanelLoaderService), typeof(PanelLoaderService), true);
            RegisterTypeIfMissing(typeof(IPanelScrollerControlViewModel), typeof(PanelScrollerControlViewModel), true);
            RegisterTypeIfMissing(typeof(IPanelsControlViewModel), typeof(PanelsControlViewModel), true);
        }
    }
}
