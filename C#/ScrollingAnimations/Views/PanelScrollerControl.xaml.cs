﻿using System.Windows.Controls;

namespace ScrollingAnimations.View
{
    /// <summary>
    /// Interaction logic for PanelScrollerControl.xaml
    /// </summary>
    public partial class PanelScrollerControl : UserControl
    {
        public PanelScrollerControl()
        {
            InitializeComponent();
        }
    }
}
