﻿using System;

namespace ScrollingAnimations.Utiltilities
{
    public class ScrollEventArgs : EventArgs
    {
        public enum Direction { Left, Right };

        public Direction ScrollDirection { get; set; }

        public ScrollEventArgs(Direction direction)
        {
            ScrollDirection = direction;
        }
    }

    public sealed class ScrollEventsPublisher
    {
        public event EventHandler<ScrollEventArgs> ScrollEvent;
        public event EventHandler<EventArgs> FinishScrollingEvent;

        internal ScrollEventsPublisher() { }

        private static readonly ScrollEventsPublisher mInstance = new ScrollEventsPublisher();
        public static ScrollEventsPublisher Instance
        {
            get {return mInstance;}
        }

        public void Raise(ScrollEventArgs e)
        {
            // Make a temporary copy of the event to avoid possibility of 
            // a race condition if the last subscriber un-subscribes 
            // immediately after the null check and before the event is raised.
            var handler = ScrollEvent;

            if (handler != null)
                handler(this, e);
        }

        public void Raise(EventArgs e)
        {
            var handler = FinishScrollingEvent;

            if (handler != null)
                handler(this, e);
        }
    }
}
