﻿using System;
using System.Windows.Input;

namespace ScrollingAnimations.Utiltilities
{
    // Taken from the following blog to allow me to define commands in View Models that can be executed from Views
    // http://social.technet.microsoft.com/wiki/contents/articles/18199.event-handling-in-an-mvvm-wpf-application.aspx
    public class DelegateCommand<T> : ICommand where T : class
    {
        private readonly Predicate<T> mCanExecute;
        private readonly Action<T> mExecute;

        public DelegateCommand(Action<T> execute, Predicate<T> canExecute)
        {
            mCanExecute = canExecute;
            mExecute = execute;
        }

        public bool CanExecute(object param)
        {
            if (mCanExecute == null)
                return true;

            return mCanExecute(param as T);
        }

        public void Execute(object param)
        {
            mExecute(param as T);
        }

        public event EventHandler CanExecuteChanged;
        public void RaiseCanExecuteChanged()
        {
            if (CanExecuteChanged != null)
                CanExecuteChanged(this, EventArgs.Empty);
        }
    }

    public class DelegateCommand : ICommand
    {
        private readonly Action mExecute;

        public DelegateCommand(Action execute)
        {
            mExecute = execute;
        }

        public bool CanExecute(object param)
        {
            return true;
        }

        public void Execute(object param)
        {
            mExecute();
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }
    }
}
