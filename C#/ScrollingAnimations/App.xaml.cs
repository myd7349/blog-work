﻿using System.Windows;

namespace ScrollingAnimations
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            var window = new View.MainWindow();

            // ViewModel to which the main window binds
            var viewModel = new ViewModel.MainWindow();

            // Bind the main window ViewModel to the main window View
            // This binding propagates down the element tree
            window.DataContext = viewModel;            

            window.Show();
        }
    }
}
