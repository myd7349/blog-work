﻿using System;
using Xunit;

namespace ScrollingAnimations.Tests
{
    public class PanelScrollerControl_Unit
    {
        [Fact]
        [Trait("Category", "Unit")]
        void Ctr_NullEventPublisher_ThrowsArgumentNullException()
        {
            Action createPanelScrollerControlAction = () => CreatePanelScrollerControl().Build();

            Assert.Throws<ArgumentNullException>("publisher", createPanelScrollerControlAction);
        }

        [Fact]
        [Trait("Category", "Unit")]
        void Ctr_MarginGreaterThanHalfButtonWidthHeight_ThrowsMemberAccessException()
        {
            var publisher = CreateScrollEventPublisher().Build();
            Action createPanelScrollerControlAction = () => CreatePanelScrollerControl()
                .WithScrollEventsPublisher(publisher)
                .WithButtonWidthHeight(3.0f)
                .WithMargin(2.0f)
                .Build();

            Assert.Throws<ArgumentException>("margin", createPanelScrollerControlAction);
        }

        [Fact]
        [Trait("Category", "Unit")]
        void NumPanels_ZeroPanelCount_ThrowsArgumentException()
        {
            var publisher = CreateScrollEventPublisher().Build();
            Action createPanelScrollerControl = () =>
                CreatePanelScrollerControl()
                .WithScrollEventsPublisher(publisher)
                .WithNumPanels(0)
                .Build();

            Assert.Throws<ArgumentException>("numPanels", createPanelScrollerControl);
        }

        [Fact]
        [Trait("Category", "Unit")]
        void ScrollLeftCommand_Invoked_RaisesScrollEvent()
        {
            var eventRaised = false;
            var publisher = CreateScrollEventPublisher().WithScrollEventHandler((o, e) => { eventRaised = true; }).Build();
            var control = CreatePanelScrollerControl().WithScrollEventsPublisher(publisher).Build();

            control.ScrollLeftCommand.Execute(this);

            Assert.True(eventRaised);
        }

        [Fact]
        [Trait("Category", "Unit")]
        void MultipleScrollLeftCommands_InvokedBeforeFirstScrollCompletes_IgnoresScrollCommandsWhileStillScrolling()
        {
            var eventRaisedCount = 0;
            var publisher = CreateScrollEventPublisher().WithScrollEventHandler((o, e) => { ++eventRaisedCount; }).Build();
            var control = CreatePanelScrollerControl().WithScrollEventsPublisher(publisher).Build();

            control.ScrollLeftCommand.Execute(this);
            control.ScrollLeftCommand.Execute(this);
            control.ScrollLeftCommand.Execute(this);

            Assert.Equal(eventRaisedCount, 1);
        }

        [Fact]
        [Trait("Category", "Unit")]
        void MultipleScrollRightCommands_InvokedAfterEachPreviousScrollCompletes_RaisesScrollEventEachTime()
        {
            var eventRaisedCount = 0;
            var publisher = CreateScrollEventPublisher().WithScrollEventHandler((o, e) => { ++eventRaisedCount; }).Build();
            var control = CreatePanelScrollerControl().WithScrollEventsPublisher(publisher).Build();

            control.ScrollLeftCommand.Execute(this);
            publisher.Raise(new EventArgs());
            control.ScrollLeftCommand.Execute(this);
            publisher.Raise(new EventArgs());
            control.ScrollLeftCommand.Execute(this);
            publisher.Raise(new EventArgs());

            Assert.Equal(eventRaisedCount, 3);
        }

        [Fact]
        [Trait("Category", "Unit")]
        void ScrollRightCommand_Invoked_RaisesScrollEvent()
        { 
            var eventRaised = false;
            var publisher = CreateScrollEventPublisher().WithScrollEventHandler((o, e) => { eventRaised = true; }).Build();
            var control = CreatePanelScrollerControl().WithScrollEventsPublisher(publisher).Build();

            control.ScrollRightCommand.Execute(this);

            Assert.True(eventRaised);
        }

        [Fact]
        [Trait("Category", "Unit")]
        void FinishScrollingHandler_InvokedAfterStartingAScroll_ClearsScrollFlags()
        {
            var publisher = CreateScrollEventPublisher().Build();
            var control = CreatePanelScrollerControl().WithScrollEventsPublisher(publisher).WithAllScrollFlagsSet().Build();

            control.ScrollRightCommand.Execute(this);
            publisher.Raise(new EventArgs());

            Assert.False(control.ScrollLeft);
            Assert.False(control.ScrollLeftWrap);
            Assert.False(control.ScrollRight);
            Assert.False(control.ScrollRightWrap);
        }

        private PanelScrollerControlBuilder CreatePanelScrollerControl()
        {
            return new PanelScrollerControlBuilder();
        }

        private ScrollEventsPublisherBuilder CreateScrollEventPublisher()
        {
            return new ScrollEventsPublisherBuilder();
        }
    }
}
