﻿using System.Collections.Generic;
using ScrollingAnimations.Services;
using ScrollingAnimations.Utiltilities;
using ScrollingAnimations.ViewModel;

namespace ScrollingAnimations.Tests
{
    public class PanelsControlBuilder
    {
        private ScrollEventsPublisher mScrollEventsPublisher;
        private SystemWrapperBase mSystemWrapper = new SystemWrapper();
        private IPanelLoaderService mPanelLoaderService = null;

        public PanelsControlBuilder WithScrollEventsPublisher(ScrollEventsPublisher publisher)
        {
            mScrollEventsPublisher = publisher;
            return this;
        }

        public PanelsControlBuilder WithPanels(List<PanelsPanel> panels)
        {
            mPanelLoaderService = new TestPanelLoaderService(panels);
            return this;
        }

        public PanelsControlBuilder WithPanelLoaderService(IPanelLoaderService service)
        {
            mPanelLoaderService = service;
            return this;
        }

        public PanelsControlBuilder WithSystemWrapper(SystemWrapperBase systemWrapper)
        {
            mSystemWrapper = systemWrapper;
            return this;
        }

        public PanelsControl Build()
        {
            return this;
        }

        public static implicit operator PanelsControl(PanelsControlBuilder builder)
        {
            var control = new PanelsControl(
                builder.mSystemWrapper,
                builder.mScrollEventsPublisher,
                builder.mPanelLoaderService,
                0.0f);

            control.HookupImagesToPanels();

            return control;
        }
    }
}
