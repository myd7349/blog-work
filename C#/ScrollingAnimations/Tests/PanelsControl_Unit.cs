﻿using System.Collections.Generic;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using ScrollingAnimations.Utiltilities;
using Xunit;

namespace ScrollingAnimations.Tests
{
    public class PanelsControl_Unit
    {
        [Fact]
        [Trait("Category", "Unit")]
        void Ctr_WithTwoImages_SetsOnScreenImageToFirstAndOffScreenImageToSecondImage()
        {
            var publisher = CreateScrollEventPublisher().Build();
            var panels = CreatePanels("image1", "image2");
            var testSystemWrapper = new TestSystemWrapper();

            var control = CreatePanelsControl().WithScrollEventsPublisher(publisher).WithPanels(panels).WithSystemWrapper(testSystemWrapper).Build();

            Assert.Equal<ImageSource>(control.OnScreenImageSource, testSystemWrapper.Images["image1"] as ImageSource);
            Assert.Equal<ImageSource>(control.OffScreenImageSource, testSystemWrapper.Images["image2"] as ImageSource);
        }

        [Fact]
        [Trait("Category", "Unit")]
        void ScrollRightEvent_Raised_UpdatesBothImageSources()
        {
            var publisher = CreateScrollEventPublisher().Build();
            var panels = CreatePanels("image1", "image2");
            var testSystemWrapper = new TestSystemWrapper();
            var control = CreatePanelsControl().WithScrollEventsPublisher(publisher).WithPanels(panels).WithSystemWrapper(testSystemWrapper).Build();

            publisher.Raise(new ScrollEventArgs(ScrollEventArgs.Direction.Right));

            Assert.Equal<ImageSource>(control.OnScreenImageSource, testSystemWrapper.Images["image2"] as ImageSource);
            Assert.Equal<ImageSource>(control.OffScreenImageSource, testSystemWrapper.Images["image1"] as ImageSource);
        }

        [Fact]
        [Trait("Category", "Unit")]
        void ScrollLeftEvent_RaisedAndFarLeftPanelSelected_WrapsAroundAndUpdatesBothImageSources()
        {
            var publisher = CreateScrollEventPublisher().Build();
            var panels = CreatePanels("image1", "image2", "image3");
            var testSystemWrapper = new TestSystemWrapper();
            var control = CreatePanelsControl().WithScrollEventsPublisher(publisher).WithPanels(panels).WithSystemWrapper(testSystemWrapper).Build();

            publisher.Raise(new ScrollEventArgs(ScrollEventArgs.Direction.Left));

            Assert.Equal<ImageSource>(control.OnScreenImageSource, testSystemWrapper.Images["image3"] as ImageSource);
            Assert.Equal<ImageSource>(control.OffScreenImageSource, testSystemWrapper.Images["image1"] as ImageSource);
        }

        [Fact]
        [Trait("Category", "Unit")]
        void MultipleScrollRightEvents_RaisedBeforeFirstScrollCompletes_IgnoresScrollEventsWhileStillScrolling()
        {
            var publisher = CreateScrollEventPublisher().Build();
            var panels = CreatePanels("image1", "image2", "image3");
            var testSystemWrapper = new TestSystemWrapper();
            var control = CreatePanelsControl().WithScrollEventsPublisher(publisher).WithPanels(panels).WithSystemWrapper(testSystemWrapper).Build();

            publisher.Raise(new ScrollEventArgs(ScrollEventArgs.Direction.Right));
            publisher.Raise(new ScrollEventArgs(ScrollEventArgs.Direction.Right));
            publisher.Raise(new ScrollEventArgs(ScrollEventArgs.Direction.Right));

            Assert.Equal<ImageSource>(control.OnScreenImageSource, testSystemWrapper.Images["image2"] as ImageSource);
            Assert.Equal<ImageSource>(control.OffScreenImageSource, testSystemWrapper.Images["image1"] as ImageSource);
        }

        [Fact]
        [Trait("Category", "Unit")]
        void MultipleScrollLeftEvents_RaisedAfterEachScrollCompletes_ScrollsToExpectedPanel()
        {
            var publisher = CreateScrollEventPublisher().Build();
            var panels = CreatePanels("image1", "image2", "image3", "image4", "image5");
            var testSystemWrapper = new TestSystemWrapper();
            var control = CreatePanelsControl().WithScrollEventsPublisher(publisher).WithPanels(panels).WithSystemWrapper(testSystemWrapper).Build();

            publisher.Raise(new ScrollEventArgs(ScrollEventArgs.Direction.Left));
            control.FinishScrollingCommand.Execute(this);
            publisher.Raise(new ScrollEventArgs(ScrollEventArgs.Direction.Left));
            control.FinishScrollingCommand.Execute(this);
            publisher.Raise(new ScrollEventArgs(ScrollEventArgs.Direction.Left));
            control.FinishScrollingCommand.Execute(this);

            Assert.Equal<ImageSource>(control.OnScreenImageSource, testSystemWrapper.Images["image3"] as ImageSource);
            Assert.Equal<ImageSource>(control.OffScreenImageSource, testSystemWrapper.Images["image4"] as ImageSource);
        }

        [Fact]
        [Trait("Category", "Unit")]
        void FinishScrollingCommand_InvokedAfterAScroll_RaisesFinishScrolllingEvent()
        {
            var finishScrollingEventRaised = false;
            var publisher = CreateScrollEventPublisher().WithFinishScrollingEventHandler((o, e) => { finishScrollingEventRaised = true; }).Build();
            var panels = CreatePanels("image1", "image2");
            var testSystemWrapper = new TestSystemWrapper();
            var control = CreatePanelsControl().WithScrollEventsPublisher(publisher).WithPanels(panels).WithSystemWrapper(testSystemWrapper).Build();

            publisher.Raise(new ScrollEventArgs(ScrollEventArgs.Direction.Right));
            control.FinishScrollingCommand.Execute(this);

            Assert.True(finishScrollingEventRaised);
        }

        private PanelsControlBuilder CreatePanelsControl()
        {
            return new PanelsControlBuilder();
        }

        private ScrollEventsPublisherBuilder CreateScrollEventPublisher()
        {
            return new ScrollEventsPublisherBuilder();
        }

        private List<PanelsPanel> CreatePanels(params string[] filenames)
        {
            var panels = new List<PanelsPanel>();

            foreach (var filename in filenames)
            {
                panels.Add(new PanelsPanel() { Background = filename });
            }
            return panels;
        }

        private class TestSystemWrapper : SystemWrapperBase
        {
            public Dictionary<string, BitmapImage> Images = new Dictionary<string, BitmapImage>();

            public override BitmapImage CreateBitmapImage(string filename)
            {
                Images[filename] = new BitmapImage();
                return Images[filename];
            }
        }
    }
}
