﻿using System;
using ScrollingAnimations.Utiltilities;

namespace ScrollingAnimations.Tests
{
    public class ScrollEventsPublisherBuilder
    {
        private event EventHandler<EventArgs> mFinishScrollingEventHandler = (o, e) => { };
        private event EventHandler<ScrollEventArgs> mScrollEventHandler = (o, e) => { };

        public ScrollEventsPublisherBuilder WithFinishScrollingEventHandler(EventHandler<EventArgs> handler)
        {
            mFinishScrollingEventHandler = handler;
            return this;
        }

        public ScrollEventsPublisherBuilder WithScrollEventHandler(EventHandler<ScrollEventArgs> handler)
        {
            mScrollEventHandler = handler;
            return this;
        }

        public ScrollEventsPublisher Build()
        {
            return this;
        }

        public static implicit operator ScrollEventsPublisher(ScrollEventsPublisherBuilder builder)
        {
            var publisher = new ScrollEventsPublisher();
            publisher.FinishScrollingEvent += builder.mFinishScrollingEventHandler;
            publisher.ScrollEvent += builder.mScrollEventHandler;
            return publisher;
        }
    }
}
