﻿using System.Windows;
using System.Windows.Controls;

namespace WPFMultiDataTrigger
{
    /// <summary>
    /// Interaction logic for ImageAndCaptionControl.xaml
    /// </summary>
    public partial class ImageAndCaptionControl : UserControl
    {
        public static readonly DependencyProperty CaptionVerticalAlignmentProperty = DependencyProperty.Register("CaptionVerticalAlignment", typeof(string), typeof(ImageAndCaptionControl), new PropertyMetadata("Top"));
        public string CaptionVerticalAlignment
        {
            get { return (string)GetValue(CaptionVerticalAlignmentProperty); }
            set { SetValue(CaptionVerticalAlignmentProperty, value); }
        }

        public ImageAndCaptionControl()
        {
            InitializeComponent();
        }
    }
}
