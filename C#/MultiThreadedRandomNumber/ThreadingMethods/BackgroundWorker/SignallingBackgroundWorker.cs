﻿using System;
using System.ComponentModel;

namespace MultiThreadedRandomNumber
{
    public sealed class SignallingBackgroundWorker : SignallingThreadingMethod
    {
        protected override void BeginCollectionPopulate(Action populateAction, Action signallingAction)
        {
            var populate = new BackgroundWorker();
            populate.DoWork += (dwO, dwE) => { populateAction(); signallingAction(); };
            populate.RunWorkerAsync();
        }

        protected override void BeginCollectionUpdate(Action updateAction, Action signallingAction)
        {
            var update = new BackgroundWorker();
            update.DoWork += (dwO, dwE) => { updateAction(); signallingAction(); };
            update.RunWorkerAsync();
        }

        protected override void BeginCollectionPrintSum(Action printSumAction, Action signallingAction)
        {
            var printSum = new BackgroundWorker();
            printSum.DoWork += (dwO, dwE) => { printSumAction(); signallingAction(); };
            printSum.RunWorkerAsync();
        }
    }
}
