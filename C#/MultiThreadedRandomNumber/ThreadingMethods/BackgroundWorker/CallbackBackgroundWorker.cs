﻿using System;
using System.ComponentModel;

namespace MultiThreadedRandomNumber
{
    public sealed class CallbackBackgroundWorker : IThreadingMethod
    {
        public void ExecuteCollectionActions(
            Action populateAction,
            Action updateAction,
            Action printSumAction,
            TimeSpan time)
        {
            // Setup the order in which the background workers should be run
            var populate = new BackgroundWorker();
            populate.DoWork += (dwO, dwE) => { mCurrentWorkerIsComplete = false; populateAction(); mCurrentWorkerIsComplete = true; };
            var lastWorkerRun = mCurrentWorker; // store the last background worker run, to check if it is still running below
            mCurrentWorker = populate;

            if (time.Seconds % 2 == 0)
            {
                var update = new BackgroundWorker();
                update.DoWork += (dwO, dwE) => { mCurrentWorkerIsComplete = false; updateAction(); mCurrentWorkerIsComplete = true; };
                mCurrentWorker.RunWorkerCompleted += (o, e) => update.RunWorkerAsync();
                mCurrentWorker = update;
            }

            if (time.Seconds % 3 == 0)
            {
                var printSum = new BackgroundWorker();
                printSum.DoWork += (dwO, dwE) => { mCurrentWorkerIsComplete = false; printSumAction(); mCurrentWorkerIsComplete = true; };
                mCurrentWorker.RunWorkerCompleted += (o, e) => printSum.RunWorkerAsync();
                mCurrentWorker = printSum;
            }

            // Execute the background workers, starting with the populate if no
            // background worker from a previous run is still executing, otherwise
            // set the populate to run once the previous background worker completes
            if (lastWorkerRun != null && !mCurrentWorkerIsComplete)
                lastWorkerRun.RunWorkerCompleted += (o, e) => populate.RunWorkerAsync();
            else
                populate.RunWorkerAsync();
        }

        private BackgroundWorker mCurrentWorker;
        private bool mCurrentWorkerIsComplete = false;
    }
}
