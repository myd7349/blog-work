﻿using System;

namespace MultiThreadedRandomNumber
{
    public interface IThreadingMethod
    {
        void ExecuteCollectionActions(
            Action populateAction,
            Action updateAction,
            Action printSumAction,
            TimeSpan time);
    }
}
