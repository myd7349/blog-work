﻿using System;
using System.Threading;

namespace MultiThreadedRandomNumber
{
    public sealed class LockingThread : IThreadingMethod
    {
        public void ExecuteCollectionActions(
            Action populateAction,
            Action updateAction,
            Action printSumAction,
            TimeSpan time)
        {
            var populate = new Thread(() => populateAction());
            populate.Start();

            if (time.Seconds % 2 == 0)
            {
                var update = new Thread(() => updateAction());
                update.Start();
            }

            if (time.Seconds % 3 == 0)
            {
                var printSum = new Thread(() => printSumAction());
                printSum.Start();
            }
        }
    }
}
