﻿using System;
using System.Threading;

namespace MultiThreadedRandomNumber
{
    // This is very similar to the SignallingThreadPool class as the ThreadPool threads
    // are setup to wait for a signal from an AutoResetEvent
    public sealed class CallbackThreadPool : IThreadingMethod
    {
        public void ExecuteCollectionActions(
            Action populateAction,
            Action updateAction,
            Action printSumAction,
            TimeSpan time)
        {
            var populate = new AutoResetEvent(false);
            if (mEventToWaitFor != null)
            {
                var eventToWaitFor = mEventToWaitFor;

                // Intentionally not storing the returned RegisteredWaitHandle as we don't care about cancellation in this case
                // and Garbage Collection performance is not an issue in this case.  It also ensures that the code is less cluttered
                ThreadPool.RegisterWaitForSingleObject(
                    eventToWaitFor,
                    (o, t) =>
                    {
                        populateAction();
                        populate.Set();
                    },
                    null,
                    -1,     // We don't want the wait to time out, rather we are waiting for the relevant AutoResetEvent to be set
                    true);  // We only want to execute the delegate once
            }
            else
            {
                ThreadPool.QueueUserWorkItem((o) => { populateAction(); populate.Set(); });
            }
            mEventToWaitFor = populate;

            if (time.Seconds % 2 == 0)
            {
                var update = new AutoResetEvent(false);

                // Not storing the returned RegisteredWaitHandle for the same reason above
                ThreadPool.RegisterWaitForSingleObject(
                    populate,
                    (o, t) =>
                    {
                        updateAction();
                        update.Set();
                    },
                    null,
                    -1,     // We don't want the wait to time out, rather we are waiting for the 'populate' AutoResetEvent to be set
                    true);  // We only want to execute the delegate once
                mEventToWaitFor = update;
            }

            if (time.Seconds % 3 == 0)
            {
                var printSum = new AutoResetEvent(false);
                var eventToWaitFor = mEventToWaitFor;

                // Not storing the returned RegisteredWaitHandle for the same reason above
                ThreadPool.RegisterWaitForSingleObject(
                    eventToWaitFor,
                    (o, t) =>
                    {
                        printSumAction();
                        printSum.Set();
                    },
                    null,
                    -1,     // We don't want the wait to time out, rather we are waiting for the relevant AutoResetEvent to be set
                    true);  // We only want to execute the delegate once
                mEventToWaitFor = printSum;
            }
        }

        private AutoResetEvent mEventToWaitFor;
    }
}