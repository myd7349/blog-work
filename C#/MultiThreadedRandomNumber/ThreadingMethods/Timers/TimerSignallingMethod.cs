﻿using System;
using System.Threading;

namespace MultiThreadedRandomNumber
{
    public sealed class TimerSingallingMethod
    {
        public void PopulateCollection(IValueCollection<uint> valueCollection, Random generator)
        {
            if (mPopulateCounter > 0)
            {
                if (mPopulateCounter % 3 == 0)
                    mPrintSum.WaitOne();
                else if (mPopulateCounter % 2 == 0)
                    mUpdate.WaitOne();
            }
            RandomNumber.Populate(valueCollection, generator);
            mPopulate.Set();
            ++mPopulateCounter;
        }

        public void UpdateCollection(IValueCollection<uint> valueCollection, Random generator)
        {
            mPopulate.WaitOne();
            RandomNumber.Update(valueCollection, generator);
            mUpdate.Set();
        }

        public void PrintCollectionSum(IValueCollection<uint> valueCollection)
        {
            if (mPrintSumCounter % 2 == 0)
                mPopulate.WaitOne();
            else
                mUpdate.WaitOne();

            RandomNumber.PrintSum(valueCollection);
            mPrintSum.Set();
            ++mPrintSumCounter;
        }

        private AutoResetEvent mPopulate = new AutoResetEvent(false);
        private AutoResetEvent mUpdate = new AutoResetEvent(false);
        private AutoResetEvent mPrintSum = new AutoResetEvent(false);

        private uint mPopulateCounter = 0;
        private uint mPrintSumCounter = 0;
    }
}
