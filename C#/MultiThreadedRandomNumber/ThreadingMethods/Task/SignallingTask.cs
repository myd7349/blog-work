﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultiThreadedRandomNumber
{
    public sealed class SignallingTask : SignallingThreadingMethod
    {
        protected override void BeginCollectionPopulate(Action populateAction, Action signallingAction)
        {
            Task.Run(() => { populateAction(); signallingAction(); });
        }

        protected override void BeginCollectionUpdate(Action updateAction, Action signallingAction)
        {
            Task.Run(() => { updateAction(); signallingAction(); });
        }

        protected override void BeginCollectionPrintSum(Action printSumAction, Action signallingAction)
        {
            Task.Run(() => { printSumAction(); signallingAction(); });
        }
    }
}
