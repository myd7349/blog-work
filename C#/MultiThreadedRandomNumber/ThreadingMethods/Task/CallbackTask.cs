﻿using System;
using System.Threading.Tasks;

namespace MultiThreadedRandomNumber
{
    public sealed class CallbackTask : IThreadingMethod
    {
        public void ExecuteCollectionActions(
            Action populateAction,
            Action updateAction,
            Action printSumAction,
            TimeSpan time)
        {
            // Setup the order in which the tasks should be run
            var populate = new Task(() => populateAction());
            var lastTaskRun = mCurrentTask;  // store the last task run, to check if it is still running below
            mCurrentTask = populate;

            if (time.Seconds % 2 == 0)
            {
                var update = mCurrentTask.ContinueWith((x) => updateAction());
                mCurrentTask = update;
            }

            if (time.Seconds % 3 == 0)
            {
                var printSum = mCurrentTask.ContinueWith((x) => printSumAction());
                mCurrentTask = printSum;
            }

            // Execute the tasks, starting with the populate if no
            // task from a previous runs is still executing, otherwise
            // set the populate to continue once the previous task completes
            if(lastTaskRun != null && !lastTaskRun.IsCanceled && !lastTaskRun.IsCompleted && !lastTaskRun.IsFaulted)
                lastTaskRun.ContinueWith((x) => populate.Start());
            else
                populate.Start();
        }

        private Task mCurrentTask;
    }
}
