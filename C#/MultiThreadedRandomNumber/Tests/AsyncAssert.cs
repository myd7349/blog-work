﻿using System;
using System.Threading.Tasks;
using Xunit;

namespace MultiThreadedRandomNumber.Tests
{
    public class ExtendedAssert
    {
        public async static Task<T> ThrowsAsync<T>(Func<Task> testCode) where T : Exception
        {
            try
            {
                await testCode();

                // Fail a standard 'Assert.Throws' if the above
                // 'testCode' doesn't throw an exception
                Assert.Throws<T>(() => { });
            }
            catch (T exception)
            {
                return exception;
            }
            return null;
        }
    }
}


