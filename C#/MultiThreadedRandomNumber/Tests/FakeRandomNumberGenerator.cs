﻿using System;
using System.Collections.Generic;

namespace MultiThreadedRandomNumber.Tests
{
    sealed class FakeRandomNumberGenerator : Random
    {
        public FakeRandomNumberGenerator(int randomNumberToProduce)
        {
            mRandomNumberList.Add(randomNumberToProduce);
        }

        public FakeRandomNumberGenerator(int[] randomNumberSequence)
        {
            mRandomNumberList.AddRange(randomNumberSequence);
        }

        public override int Next(int minValue, int maxValue)
        {
            var result = mRandomNumberList[mCurrentIndex];

            lock (mSynchronisationObject)
            {
                mCurrentIndex = (mCurrentIndex + 1 >= mRandomNumberList.Count) ? 0 : ++mCurrentIndex;
            }

            if (result < minValue)
                result = minValue;
            if (result > maxValue)
                result = maxValue;

            return result;
        }

        private List<int> mRandomNumberList = new List<int>();
        private int mCurrentIndex = 0;
        private object mSynchronisationObject = new object();
    }
}
