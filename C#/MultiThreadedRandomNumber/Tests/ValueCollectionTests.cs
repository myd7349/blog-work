﻿using System.Collections.Generic;
using Xunit;
using Xunit.Extensions;

namespace MultiThreadedRandomNumber.Tests
{
    public class ValueCollectionTests
    {
        [Theory, MemberData("FloatValueCollections")]
        public void add_unique_returns_true_when_adding_a_unique_value(IValueCollection<float> collection)
        {
            // Given:
            // When:
            bool valueAdded = collection.AddUnique(3.0f);

            // Then:
            Assert.True(valueAdded);
        }

        [Theory, MemberData("IntValueCollections")]
        public void count_returns_0_for_empty_collection(IValueCollection<int> collection)
        {
            // Given:
            const int expectedCount = 0;

            // When:
            // Then:
            Assert.Equal(expectedCount, collection.Count);
        }

        [Theory, MemberData("IntValueCollections")]
        public void count_returns_2_for_collection_containing_2_floats_number(IValueCollection<int> collection)
        {
            // Given:
            const int expectedCount = 2;

            // When:
            collection.AddUnique(1);
            collection.AddUnique(2);

            // Then:
            Assert.Equal(expectedCount, collection.Count);
        }

        [Theory, MemberData("FloatValueCollections")]
        public void add_unique_ensures_collection_only_contains_unique_values(IValueCollection<float> collection)
        {
            // Given:
            const int expectedCount = 2;
            const float firstValue = 1.7f;
            const float secondValue = 2.3f;

            // When:
            collection.AddUnique(firstValue);
            collection.AddUnique(secondValue);
            bool duplicateValueAdded = collection.AddUnique(firstValue);

            // Then:
            Assert.False(duplicateValueAdded);
            Assert.Equal(expectedCount, collection.Count);
            Assert.Contains(firstValue, collection.GetValueEnumerable());
            Assert.Contains(secondValue, collection.GetValueEnumerable());
        }

        [Theory, MemberData("IntValueCollections")]
        public void get_value_enumerable_returns_an_enumerable_equivalent_to_manually_created_enumerable(IValueCollection<int> collection)
        {
            // Given:
            const int firstValue = 3;
            const int secondValue = 7;
            var manuallyCreatedEnumberable = new List<int>();
            manuallyCreatedEnumberable.AddRange(new int[] { firstValue, secondValue });

            // When:
            collection.AddUnique(firstValue);
            collection.AddUnique(secondValue);

            // Then:
            Assert.Equal(manuallyCreatedEnumberable, collection.GetValueEnumerable());
        }

        [Theory, MemberData("FloatValueCollections")]
        public void clear_empties_the_collection(IValueCollection<float> collection)
        {
            // Given:
            const int expectedCount = 0;
            const float firstValue = 1.7f;
            const float secondValue = 2.3f;

            // When:
            collection.AddUnique(firstValue);
            collection.AddUnique(secondValue);
            collection.Clear();

            // Then:
            Assert.Equal(expectedCount, collection.Count);
        }

        [Theory, MemberData("IntValueCollections")]
        public void remove_deletes_expected_element_when_passing_a_valid_index(IValueCollection<int> collection)
        {
            // Given:
            const int indexToRemove = 2;
            const int valueToRemoveAtIndex = 233;
            int valueRemoved = 0;

            // When:
            collection.AddUnique(6);
            collection.AddUnique(3);
            collection.AddUnique(valueToRemoveAtIndex);
            collection.AddUnique(245);
            bool valueRemovedFromCollection = collection.Remove(indexToRemove, ref valueRemoved);

            // Then:
            Assert.True(valueRemovedFromCollection);
            Assert.Equal(valueToRemoveAtIndex, valueRemoved);
        }

        public static IEnumerable<object[]> FloatValueCollections
        {
            get
            {
                yield return new object[] { new LockFreeValueCollection<float>() };
                yield return new object[] { new LockingValueCollection<float>() };
            }
        }

        public static IEnumerable<object[]> IntValueCollections
        {
            get
            {
                yield return new object[] { new LockFreeValueCollection<int>() };
                yield return new object[] { new LockingValueCollection<int>() };
            }
        }
    }
}
