﻿using System;
using System.Collections.Generic;

namespace MultiThreadedRandomNumber
{
    public interface IValueCollection<T>
        // Attempt to limit the generic type parameters to be numeric types
        where T : struct, IComparable, IFormattable, IConvertible, IComparable<T>, IEquatable<T>
    {
        int Count { get; }
        bool AddUnique(T value);
        bool Remove(int index, ref T value);
        void Clear();
        IEnumerable<T> GetValueEnumerable();
    };
}
