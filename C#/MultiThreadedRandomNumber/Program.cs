﻿using System;
using System.Threading;

namespace MultiThreadedRandomNumber
{
    using System.Threading.Tasks;
    using sm = RandomNumber.SynchronisationMethod;
    using tm = RandomNumber.ThreadingMethod;

    class Program
    {
        static void Main(string[] args)
        {
            var tMethod = tm.ThreadPool;
            var sMethod = sm.Callback;
            int oneSecond = 1000;            
            var generator = new Random();

            ParseCommandLineArgs(args, tMethod, sMethod);
            var valueCollection = RandomNumber.CreateCollection<uint>(sm.Locking == sMethod);
            var threadingMethod = RandomNumber.CreateThreadingMethod(tMethod, sMethod);

            if (tMethod == tm.Timer)
            {
                var newTimerMethods = new TimerSingallingMethod();
                var populateTimer = new System.Threading.Timer(
                    (o) => (o as TimerSingallingMethod).PopulateCollection(valueCollection, generator),
                    newTimerMethods,
                    0,
                    oneSecond);

                var updateTimer = new System.Threading.Timer(
                    (o) => (o as TimerSingallingMethod).UpdateCollection(valueCollection, generator),
                    newTimerMethods,
                    0,
                    oneSecond * 2);

                var printSumTimer = new System.Threading.Timer(
                    (o) => (o as TimerSingallingMethod).PrintCollectionSum(valueCollection),
                    newTimerMethods,
                    0,
                    oneSecond * 3);
            }
            else
            {
                var timer = new System.Timers.Timer(oneSecond);
                timer.Enabled = true;
                var startingDateTime = DateTime.Now;

                // Each time the interval elapses the selected threading
                // method executes the various actions asynchronously
                timer.Elapsed += (o, e) =>
                {
                    var difference = (e.SignalTime - startingDateTime);

                    threadingMethod.ExecuteCollectionActions(
                        () => RandomNumber.Populate(valueCollection, generator),
                        () => RandomNumber.Update(valueCollection, generator),
                        () => RandomNumber.PrintSum(valueCollection),
                        difference);
                };
            }

            Console.WriteLine("Press the Enter key to exit the program.");
            Console.ReadLine();
        }

        static void ParseCommandLineArgs(string[] args, tm tMethod, sm sMethod)
        {
            if (args.Length > 0)
            {
                switch (args[0])
                {
                    case "bw": tMethod = tm.BackgroundWorker; break;
                    case "task": tMethod = tm.Task; break;
                    case "thread": tMethod = tm.Thread; break;
                    case "thread_pool": tMethod = tm.ThreadPool; break;
                    case "timer": tMethod = tm.Timer; break;
                }
            }

            if (args.Length > 1)
            {
                switch (args[1])
                {
                    case "callback": sMethod = sm.Callback; break;
                    case "locking": sMethod = sm.Locking; break;
                    case "signalling": sMethod = sm.Signalling; break;
                }
            }
        }
    }
}
