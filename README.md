# README #

### What is this repository for? ###

This repository will contain the code that accompanies the various blog posts that I intend on writing on www.eoinoflynn.com

### How do I get set up? ###

The quickest way to get access to the code is to download it from the 'Navigation - Downloads' menu in the left index.  This will give you the entire repository.  Using Windows Explorer to open the downloaded repository zip file won't work.  I managed to get it working using 7-zip.

The root directory for the repository contains a directory for each programming language that I write a blog post for (e.g. C#, C++, Python etc.).  It also contains a 'Third Party' directory that stores the dependencies on any third party software.  The 'bin' directory inside the 'Third Party' directory has the relevant binaries.

I use Visual Studio 2013 as the IDE for development.  In each language folder the 'packages' directory contains the Nuget packages that I have installed for the various projects.  No binaries are submitted to the repository from the language folders.

You should be able to sync everything that is required to build each project.  Let me know if that isn't the case.