def addValues(a, b):
    if a+1 == a:
        raise OverflowError("a is too large")
    if b+1 == b:
        raise OverflowError("b is too large")
    if a+b+1 == a+b:
        raise OverflowError("The sum of a & b is too large")
    return a + b