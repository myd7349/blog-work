import unittest
import SimplePyMEL
import SimplePython
import SimplePyMELTest
import SimplePythonTest

# Load the first suite of tests
from SimplePyMELTest import SimplePyMELTest as testCase1
suite1 = unittest.TestLoader().loadTestsFromTestCase(testCase1)

# Load the second suite of tests
from SimplePythonTest import SimplePythonTest as testCase2
suite2 = unittest.TestLoader().loadTestsFromTestCase(testCase2)

# Run all of the tests storing the output
allTests = unittest.TestSuite([suite1, suite2])
unittest.TextTestRunner(verbosity=2).run(allTests)