#include "binary_operations.h"

#include <algorithm>
#include <cmath>
#include <sstream>

using namespace BinaryFloatingPoint;
using namespace std;

namespace
{
	const size_t A = 'A';
	const string One("1");
	const string Zero("0");
	const size_t MantissaSize = 24;
	const size_t ExponentSize = 8;

	// IEEE standard using 32 bits:
	// - 1 bit for the sign
	// - 8 bits for the position of the radix point (-127 to 127)
	// - 23 bits for mantissa (first significant digit of binary 
	// is always 1 and therefore excluded to allow for more precision)
	string convertFloatTo32BitIEEEString(const float value);
	bool getIntegerAndFractionalPartsFrom32BitIEEEString(const string& value, string& integerPart, string& fractionalPart);

	string convertIntegerPartToBinary(const float value);
	float convertIntegerPartFromBinary(string& value);
	string convertFractionalPartToBinary(const float value, const size_t allowedNumBits);
	float convertFractionalPartFromBinary(string& value);

	void pad(string& stringToPad, size_t maxLength, const bool padLeft, const char padChar);
	void trimZeros(string& stringToTrim, const bool trimLeft);
	float addFloats(const string& leftInteger, const string& rightInteger, const string& leftFractional, const string& rightFractional, const bool leftIsPositive, const bool rightIsPositive);
	float subtractFloats(const string& leftInteger, const string& rightInteger, const string& leftFractional, const string& rightFractional);
	bool binaryFloatGreaterThan(const string& leftIntegerPart, const string& leftFractionalPart, const string& rightIntegerPart, const string& rightFractionalPart);
	bool leftIsLargerThanRight(const string& leftBinaryValue, const string& rightBinaryValue);
	bool isZero(const string& ieeeFloat);
	bool borrowOnes(string& value, const size_t subStringLength);
	void reverseString(std::string& s);

	string addBinaryIntegers(const string& left, const string& right);
	string addBinaryFractions(const string& left, const string& right, bool& carry);

	string subtractBinaryIntegers(const string& left, const string& right);
	string subtractBinaryFractions(const string& left, const string& right, bool& borrow);

	float multiplyBinaryIntegers(const string& left, const string& right);
	float multiplyBinaryFractions(const string& left, const string& right);
	float multiplyBinaryIntegerAndFraction(const string& integer, const string& fraction);

	float divideBinaryFloatByBinaryInteger(const string& numerator, const string& denominator, const size_t decimalPosition);
}

namespace maths
{
	size_t floor(const float value)
	{
		return static_cast<size_t>(value);
	}

	size_t ceil(const float value)
	{
		return floor(value) + 1;
	}

	bool isNegative(const float value)
	{
		return value < 0.0;
	}

	template <typename T>
	T min(T left, T right)
	{
		return (left <= right) ? left : right;
	}

	template <typename T>
	T max(T left, T right)
	{
		return (left >= right) ? left : right;
	}
}

namespace BinaryFloatingPoint
{
	bool convert_base(string& number, const size_t currentBase, const size_t wantedBase)
	{
		if (number.empty() || currentBase == wantedBase)
			return false;

		if (currentBase < 2 || currentBase > 16 || wantedBase < 2 || wantedBase > 16)
			return false;

		// Convert from current base to base 10 if required
		auto numberInWantedBase = number;
		if (wantedBase != 10 && currentBase != 10)
			numberInWantedBase = convert_base(number, currentBase, 10);

		const size_t zero = '0';
		size_t value = 0;
		size_t power = 0;

		// Calculate the value of the number string
		for (auto it = numberInWantedBase.crbegin(); it != numberInWantedBase.crend(); ++it)
		{
			auto digit = static_cast<size_t>(*it);
			size_t digitValue;

			if (digit >= A)
				digitValue = 10 + digit - A;
			else
				digitValue = digit - zero;

			value += digitValue * static_cast<size_t>(pow(currentBase, power++));
		}

		number = convert_base(value, currentBase, wantedBase);

		return true;
	}

	string convert_base(size_t value, const size_t currentBase, const size_t wantedBase)
	{
		stringstream resultSS;
		const auto useLetters = (wantedBase > 10);

		// Convert value to wantedBase
		while (value >= wantedBase)
		{
			auto remainder = value % wantedBase;
			value = ((value - remainder) / wantedBase);
			resultSS << ((useLetters && remainder > 9) ? static_cast<char>(A + remainder - 10) : remainder);
		}
		resultSS << ((useLetters && value > 9) ? static_cast<char>(A + value - 10) : value);
		auto number = resultSS.str();

		// Reverse the order of the characters as the conversion above places the
		// characters in ascending order from left to right in the stringstream
		reverseString(number);

		return number;
	}

	float add_ieee(const float left, const float right)
	{
		const auto leftString = convertFloatTo32BitIEEEString(left);
		const auto rightString = convertFloatTo32BitIEEEString(right);

		if (isZero(leftString))
			return right;
		else if (isZero(rightString))
			return left;

		// Starting point of the addition operation. We need to split the strings into the relevant integer and fractional parts
		string integerPart[2], fractionalPart[2];
		const auto leftIsPositive = getIntegerAndFractionalPartsFrom32BitIEEEString(leftString, integerPart[0], fractionalPart[0]);
		const auto rightIsPositive = getIntegerAndFractionalPartsFrom32BitIEEEString(rightString, integerPart[1], fractionalPart[1]);
		
		if (leftIsPositive ^ rightIsPositive)
		{
			const auto leftIndex = (leftIsPositive) ? 0 : 1;
			const auto rightIndex = (leftIsPositive) ? 1 : 0;
			return subtractFloats(integerPart[leftIndex], integerPart[rightIndex], fractionalPart[leftIndex], fractionalPart[rightIndex]);
		}
		else
		{
			return addFloats(integerPart[0], integerPart[1], fractionalPart[0], fractionalPart[1], leftIsPositive, rightIsPositive);
		}
	}

	float subtract_ieee(const float left, const float right)
	{
		const auto leftString = convertFloatTo32BitIEEEString(left);
		const auto rightString = convertFloatTo32BitIEEEString(right);

		if (isZero(leftString))
			return right * -1.0f;
		else if (isZero(rightString))
			return left;

		// Starting point of the subtraction operation. We need to split the strings into the relevant integer and fractional parts
		string integerPart[2], fractionalPart[2];
		const auto leftIsPositive = getIntegerAndFractionalPartsFrom32BitIEEEString(leftString, integerPart[0], fractionalPart[0]);
		const auto rightIsPositive = getIntegerAndFractionalPartsFrom32BitIEEEString(rightString, integerPart[1], fractionalPart[1]);

		if (leftIsPositive ^ rightIsPositive)
			return addFloats(integerPart[0], integerPart[1], fractionalPart[0], fractionalPart[1], leftIsPositive, rightIsPositive);
		else
			return subtractFloats(integerPart[0], integerPart[1], fractionalPart[0], fractionalPart[1]);
	}
	
	float multiply_ieee(const float left, const float right)
	{
		const auto leftString = convertFloatTo32BitIEEEString(left);
		const auto rightString = convertFloatTo32BitIEEEString(right);

		if (isZero(leftString) || isZero(rightString))
			return 0.0f;

		// Starting point of the multiplication operation. We need to split the strings into the relevant integer and fractional parts
		string integerPart[2], fractionalPart[2];
		const auto leftIsPositive = getIntegerAndFractionalPartsFrom32BitIEEEString(leftString, integerPart[0], fractionalPart[0]);
		const auto rightIsPositive = getIntegerAndFractionalPartsFrom32BitIEEEString(rightString, integerPart[1], fractionalPart[1]);

		auto result = multiplyBinaryIntegers(integerPart[0], integerPart[1]);
		result += multiplyBinaryFractions(fractionalPart[0], fractionalPart[1]);
		result += multiplyBinaryIntegerAndFraction(integerPart[0], fractionalPart[1]);
		result += multiplyBinaryIntegerAndFraction(integerPart[1], fractionalPart[0]);

		if (leftIsPositive ^ rightIsPositive)
			result *= -1.0f;

		return result;
	}

	bool divide_ieee(const float numerator, const float denominator, float& result)
	{
		result = 0.0f;
		const auto numeratorString = convertFloatTo32BitIEEEString(numerator);
		const auto denominatorString = convertFloatTo32BitIEEEString(denominator);

		if (isZero(numeratorString))
		{
			result = 0.0f;
			return true;
		}
		else if (isZero(denominatorString))
		{
			return false;
		}

		// Starting point of the division operation. We need to split the strings into the relevant integer and fractional parts
		string integerPart[2], fractionalPart[2];
		const auto leftIsPositive = getIntegerAndFractionalPartsFrom32BitIEEEString(numeratorString, integerPart[0], fractionalPart[0]);
		const auto rightIsPositive = getIntegerAndFractionalPartsFrom32BitIEEEString(denominatorString, integerPart[1], fractionalPart[1]);

		const auto decimalPosition = integerPart[0].length();
		if (integerPart[1] != Zero && fractionalPart[1].find('1') == string::npos)
		{
			result = divideBinaryFloatByBinaryInteger(integerPart[0] + fractionalPart[0], integerPart[1], decimalPosition);
		}
		else
		{
			const auto lastOne = fractionalPart[1].find_last_of('1');
			if (lastOne == string::npos)
				return false;

			// Multiply the fractional part of the denominator by 2 until it is a whole integer
			const auto scale = maths::min(lastOne + 1, MantissaSize - decimalPosition);
			string scaledFractionDenominator(fractionalPart[1].substr(0, scale));
			string trimmedDenominator(integerPart[1] + scaledFractionDenominator);
			trimZeros(trimmedDenominator, true);

			// Remove any 0s after the last 1 in the numerator as the fractionalPart is automatically padded with 0s
			string trimmedNumerator(integerPart[0] + fractionalPart[0]);
			trimZeros(trimmedNumerator, false);

			// Multiply the numerator by the same number of 2s as the denominator above
			size_t newDecimalPosition = decimalPosition + scale;
			if (trimmedNumerator.length() < newDecimalPosition)
			{
				const auto padLength = newDecimalPosition - trimmedNumerator.length();
				trimmedNumerator += string(padLength, '0');
			}

			result = divideBinaryFloatByBinaryInteger(trimmedNumerator, trimmedDenominator, newDecimalPosition);
		}

		return true;
	}
}

namespace
{
	// Method for conversion:
	// 1. Translate both sides of the radix into binary
	// 2. Translate that into scientific notation
	// 3. Translate the exponent into binary and add/subtract 127 depending on sign
	// 4. Pad the mantissa/significand
	// 5. Ignore the first significant digit of the mantissa (it is always 1)
	// 6. Add final bit for sign
	string convertFloatTo32BitIEEEString(const float value)
	{
		if (value == 0.0f)
		{
			string exponent("127");
			convert_base(exponent, 10, 2);
			pad(exponent, ExponentSize, true, '0');
			string mantissa(MantissaSize - 1, '0');
			return string(Zero + exponent + mantissa);
		}

		const bool isPositive = value > 0.0f;
		const string zero(Zero);

		// Step 1.
		auto integerPart = convertIntegerPartToBinary(value);
		auto fractionalPart = convertFractionalPartToBinary(value, MantissaSize);

		// Step 2.
		int exponent;
		if (integerPart == zero)
		{
			integerPart.clear();
			exponent = (fractionalPart.find_first_of('1') + 1) * -1; // e.g. 0.0023 has an exponent of -3 => 2.3 e ^ -3
			fractionalPart.erase(0, abs(exponent) - 1);
		}
		else
		{
			exponent = integerPart.length();
		}

		// Step 3.
		exponent += 127;
		string binaryExponent(convert_base(exponent, 10, 2));
		pad(binaryExponent, ExponentSize, true, '0');

		// Step 4.
		string mantissa(integerPart + fractionalPart);
		if (mantissa.length() > MantissaSize)
		{
			mantissa.erase(MantissaSize);
		}
		else
		{
			pad(mantissa, MantissaSize, false, '0');
		}

		// Step 5.
		mantissa.erase(0, 1);

		// Step 6.
		string result;
		if (isPositive)
			result = zero + binaryExponent + mantissa;
		else
			result = "1" + binaryExponent + mantissa;

		return result;
	}

	// Produces the integer and fraction parts of a float as two strings the length of both equates to MantissaSize
	bool getIntegerAndFractionalPartsFrom32BitIEEEString(const string& value, string& integerPart, string& fractionalPart)
	{
		// Sign
		const auto signBit = value[0];
		const bool isPositive = (signBit == '0');

		// Exponent
		auto exponent = value.substr(1, ExponentSize);
		convert_base(exponent, 2, 10);
		auto exponentValue = stoi(exponent) - 127;

		// Mantissa
		auto mantissa = "1" + value.substr(9); // add the implicit '1' back in
		if (exponentValue > 0)
		{
			integerPart = mantissa.substr(0, exponentValue);
			fractionalPart = mantissa.substr(exponentValue);
		}
		else if (exponentValue < 0)
		{
			integerPart = Zero;
			fractionalPart = (string(abs(exponentValue) - 1, '0') + mantissa);

			// Maintain mantissa size
			fractionalPart.erase(MantissaSize - 1);
		}
		else
		{
			integerPart = Zero;
			fractionalPart = Zero;
		}

		return isPositive;
	}

	string convertIntegerPartToBinary(const float value)
	{
		const auto integerPart = maths::floor(abs(value));
		const auto integerPartAsString = convert_base(integerPart, 10, 2);

		return integerPartAsString;
	}

	float convertIntegerPartFromBinary(string& value)
	{
		convert_base(value, 2, 10);

		return stof(value);
	}

	string convertFractionalPartToBinary(const float value, const size_t allowedNumBits)
	{
		string fractionalPart("");
		auto remainder = abs(value) - maths::floor(abs(value));

		// Iterate until we reach 0 or the until the precision limit is reached for the mantissa
		while (remainder > 0.0 && fractionalPart.length() <= allowedNumBits)
		{
			remainder *= 2.0f;	// base 2 for binary
			auto digit = maths::floor(remainder);
			fractionalPart.append(to_string(digit));
			remainder -= digit;
		}

		return fractionalPart;
	}

	float convertFractionalPartFromBinary(string& value)
	{
		int power = 1;
		float result = 0.0f;
		for (auto digit : value)
		{
			result += ((digit == '1') ? (1.0f / pow(2.0f, power)) : 0.0f);
			++power;
		}
		return result;
	}

	void pad(string& stringToPad, const size_t maxLength, const bool padLeft, const char padChar)
	{
		string pad(maxLength - stringToPad.length(), padChar);
		if (padLeft)
			stringToPad = pad + stringToPad;
		else
			stringToPad += pad;
	}

	void trimZeros(string& stringToTrim, const bool trimLeft)
	{
		if (trimLeft)
		{
			auto firstOne = stringToTrim.find_first_of('1');
			if (firstOne == string::npos)
				stringToTrim = "";
			else
				stringToTrim = stringToTrim.substr(firstOne);
		}
		else
		{
			auto lastOne = stringToTrim.find_last_of('1');
			if (lastOne == string::npos)
				stringToTrim = "";
			else
				stringToTrim = stringToTrim.substr(0, lastOne + 1);
		}
	}

	float addFloats(const string& leftInteger, const string& rightInteger, const string& leftFractional, const string& rightFractional, const bool leftIsPositive, const bool rightIsPositive)
	{
		// Add the fraction parts
		auto carry = false;
		auto resultFractionalPart = addBinaryFractions(leftFractional, rightFractional, carry);
		auto result = convertFractionalPartFromBinary(resultFractionalPart);

		// Add the integer parts
		auto resultIntegerPart = addBinaryIntegers(leftInteger, rightInteger);
		
		// Add 1 from the result of adding the fractional parts if required
		if (carry)
			resultIntegerPart = addBinaryIntegers(resultIntegerPart, One);

		result += convertIntegerPartFromBinary(resultIntegerPart);

		if (!leftIsPositive && !rightIsPositive)
			result *= -1.0f;

		return result;
	}

	float subtractFloats(const string& leftInteger, const string& rightInteger, const string& leftFractional, const string& rightFractional)
	{
		// Calculate which side is the largest and place it on the LHS
		const auto leftIsLarger = binaryFloatGreaterThan(leftInteger, leftFractional, rightInteger, rightFractional);
		auto& largerInteger = (leftIsLarger) ? leftInteger : rightInteger;
		auto& smallerInteger = (leftIsLarger) ? rightInteger : leftInteger;
		auto& largerFraction = (leftIsLarger) ? leftFractional : rightFractional;
		auto& smallerFraction = (leftIsLarger) ? rightFractional : leftFractional;

		// Subtract the fractional parts
		bool borrow = false;
		auto resultFractionalPart = subtractBinaryFractions(largerFraction, smallerFraction, borrow);
		auto result = convertFractionalPartFromBinary(resultFractionalPart);

		// Subtract the integer parts
		auto resultIntegerPart = subtractBinaryIntegers(largerInteger, smallerInteger);

		// Subtract 1 from the result if required
		if (borrow)
			resultIntegerPart = subtractBinaryIntegers(resultIntegerPart, One);

		result += convertIntegerPartFromBinary(resultIntegerPart);

		if (!leftIsLarger)
			result *= -1.0f;

		return result;
	}

	bool binaryFloatGreaterThan(const string& leftIntegerPart, const string& leftFractionalPart, const string& rightIntegerPart, const string& rightFractionalPart)
	{
		const auto leftLength = leftIntegerPart.length();
		const auto rightLength = rightIntegerPart.length();

		// The integer strings are only as long as the position of the most significant bit
		// Therefore comparing string length is a quick indicator to find which one is greater
		if (leftLength > rightLength)
		{
			return true;
		}
		else if (leftLength < rightLength)
		{
			return false;
		}
		else
		{
			if (leftIntegerPart == rightIntegerPart)
			{
				// Integer parts are equal so compare the fractional parts to see which side is the
				// largest by finding the position of the most significant bit after the decimal point
				const auto firstLeftOne = leftFractionalPart.find('1');
				const auto firstRightOne = rightFractionalPart.find('1');
				if (firstLeftOne < firstRightOne)
					return true;
				else if (firstLeftOne > firstRightOne)
					return false;
				else
					return leftIsLargerThanRight(leftFractionalPart, rightFractionalPart);
			}
			else
			{
				// As the integer parts are different we can ignore the fractional parts
				return leftIsLargerThanRight(leftIntegerPart, rightIntegerPart);
			}
		}
	}

	bool leftIsLargerThanRight(const string& leftBinaryValue, const string& rightBinaryValue)
	{
		string leftRemainder(leftBinaryValue);
		string rightRemainder(rightBinaryValue);
		size_t nextLeftOne = 0, nextRightOne = 0;

		while (nextLeftOne == nextRightOne)
		{
			leftRemainder = leftRemainder.substr(nextLeftOne + 1);
			rightRemainder = rightRemainder.substr(nextRightOne + 1);
			nextLeftOne = leftRemainder.find('1');
			nextRightOne = rightRemainder.find('1');

			if (nextLeftOne == string::npos)
				return false;
			else if (nextLeftOne != string::npos && nextRightOne == string::npos)
				return true;
		}

		// The position of the most significant bits are the same so we have to iterate from the 
		// most significant bit (i.e. right - left) to find the position of the next 1
		return (nextLeftOne < nextRightOne);
	}

	bool isZero(const string& ieeeFloat)
	{
		return ieeeFloat == "00111111100000000000000000000000";
	}

	bool borrowOnes(string& value, const size_t subStringLength)
	{
		string rangeToSearch(value.substr(0, subStringLength));
		const auto rangeBegin = rangeToSearch.find_last_of('1');

		if (rangeBegin != string::npos)
		{
			value[rangeBegin] = '0';
			for (auto i = rangeBegin + 1; i < subStringLength; ++i)
			{
				value[i] = '1';
			}

			return true;
		}

		return false;
	}

	void reverseString(std::string& s)
	{
		const auto length = s.length();
		if (length == 0)
			return;

		// 0 based indexing
		size_t charsLeft = length - 1;

		for (size_t i = 0; i < charsLeft; ++i, --charsLeft)
		{
			s[i] ^= s[charsLeft];
			s[charsLeft] ^= s[i];
			s[i] ^= s[charsLeft];
		}
	}

	string addBinaryIntegers(const string& left, const string& right)
	{
		bool carry = false;
		const int leftLength = left.length();
		const int rightLength = right.length();
		const auto length = ((leftLength > rightLength) ? leftLength : rightLength);
		string result(length, '0');

		// Iterate through the characters from right to left, matching up the end (rhs) of both strings
		for (int l = leftLength - 1, r = rightLength - 1, i = length - 1; i >= 0; --l, --r, --i)
		{
			const auto leftChar = (l < 0) ? '0' : left[l];
			const auto rightChar = (r < 0) ? '0' : right[r];
			result[i] = '0' + ((carry) ? (1 ^ leftChar ^ rightChar) : (leftChar ^ rightChar));
			carry = (carry) ? (('1' & (leftChar | rightChar)) == '1') : ((leftChar & rightChar) == '1');
		}

		if (carry)
			pad(result, length + 1, true, '1');

		return result;
	}

	string addBinaryFractions(const string& left, const string& right, bool& carry)
	{
		carry = false;
		const int leftLength = left.length();
		const int rightLength = right.length();
		const auto length = ((leftLength > rightLength) ? leftLength : rightLength);
		string result(length, '0');

		// Iterate through the characters from right to left, matching up the beginning (lhs) of both strings
		for (auto i = length - 1; i >= 0; --i)
		{
			const auto leftChar = (i >= leftLength) ? '0' : left[i];
			const auto rightChar = (i >= rightLength) ? '0' : right[i];
			result[i] = '0' + ((carry) ? (1 ^ leftChar ^ rightChar) : (leftChar ^ rightChar));
			carry = (carry) ? ((leftChar | rightChar) == '1') : ((leftChar & rightChar) == '1');
		}

		return result;
	}

	string subtractBinaryIntegers(const string& left, const string& right)
	{
		string topInteger(left);
		const int leftLength = left.length();
		const int rightLength = right.length();
		const auto length = ((leftLength > rightLength) ? leftLength : rightLength);
		string result(length, '0');
		auto carry = false;

		// Iterate through the characters from right to left, matching up the end (rhs) of both strings
		for (auto l = leftLength - 1, r = rightLength - 1, i = length - 1; i >= 0; --l, --r, --i)
		{
			const auto leftChar = (l < 0) ? '0' : topInteger[l];
			const auto rightChar = (r < 0) ? '0' : right[r];

			if (leftChar == '0' && rightChar == '1')
			{
				borrowOnes(topInteger, l);
				result[i] = '1';
			}
			else
			{
				result[i] = '0' + (leftChar ^ rightChar);
			}
		}

		return result;
	}

	string subtractBinaryFractions(const string& left, const string& right, bool& borrow)
	{
		string topInteger(left);
		const int leftLength = left.length();
		const int rightLength = right.length();
		const auto length = ((leftLength > rightLength) ? leftLength : rightLength);
		string result(length, '0');

		// Iterate through the characters from right to left, matching up the beginning (lhs) of both strings
		for (auto i = length - 1; i >= 0; --i)
		{
			const auto leftChar = (i >= leftLength) ? '0' : topInteger[i];
			const auto rightChar = (i >= rightLength) ? '0' : right[i];

			if (leftChar == '0' && rightChar == '1')
			{
				if (!borrowOnes(topInteger, i))
					borrow = true;
				result[i] = '1';
			}
			else
			{
				result[i] = '0' + (leftChar ^ rightChar);
			}
		}

		return result;
	}

	float multiplyBinaryIntegers(const string& left, const string& right)
	{
		const int rightLength = right.length();
		string result(Zero);
		auto scale = 0;

		// Iterate through the characters from right to left, matching up the last digit on (rhs) of both factors
		for (auto r = rightLength - 1; r >= 0; --r)
		{
			if (right[r] == '1')
			{
				const string zeros(scale, '0');
				const auto valueToAdd = left + zeros;
				result = addBinaryIntegers(result, valueToAdd);
			}

			++scale;
		}

		return convertIntegerPartFromBinary(result);
	}

	float multiplyBinaryFractions(const string& left, const string& right)
	{
		string result;
		auto scale = 1;

		// Iterate through the characters from left to right, matching up the first digit on (lhs) of both factors
		for (const auto rightDigit : right)
		{
			if (rightDigit == '1')
			{
				const string zeros(scale, '0');
				const auto valueToAdd = zeros + left;
				bool carry = false;
				result = addBinaryFractions(result, valueToAdd, carry);
			}

			++scale;
		}

		return convertFractionalPartFromBinary(result);
	}

	float multiplyBinaryIntegerAndFraction(const string& integer, const string& fraction)
	{
		// Ensure that the integer is at least as long as the fraction
		auto integerLength = integer.length();
		auto fractionLength = fraction.length();
		string paddedInteger(integer);
		if (fractionLength > integerLength)
		{
			paddedInteger = string(fractionLength - integerLength, '0') + paddedInteger;
			integerLength = paddedInteger.length();
		}

		string integerResult(Zero), fractionResult(Zero);
		auto fractionDigitPos = 1;

		// Iterate through each fraction digit updating both result strings each time we encounter a 1
		for (const auto fractionDigit : fraction)
		{
			if (fractionDigit == '1')
			{
				const auto scaledInteger = paddedInteger.substr(0, integerLength - fractionDigitPos);
				integerResult = addBinaryIntegers(integerResult, scaledInteger);

				bool carry = false;
				const auto scaledFraction = paddedInteger.substr(integerLength - fractionDigitPos);
				fractionResult = addBinaryFractions(fractionResult, scaledFraction, carry);

				if (carry)
					integerResult = addBinaryIntegers(integerResult, One);
			}

			++fractionDigitPos;
		}

		return convertIntegerPartFromBinary(integerResult) + convertFractionalPartFromBinary(fractionResult);
	}

	float divideBinaryFloatByBinaryInteger(const string& numerator, const string& denominator, const size_t decimalPosition)
	{
		string integerResult, fractionResult;
		string remainder(numerator), dividend;
		const auto numeratorLength = numerator.length();
		size_t index = 1;

		// Keep iterating until the result reaches the maximum mantissa size
		while (integerResult.length() + fractionResult.length() < MantissaSize)
		{
			auto divides = false;
			dividend = remainder.substr(0, index);

			// The denominator can only divide the dividend if the dividend is at least as long as the denominator
			if (dividend.length() >= denominator.length())
			{
				string trimmedDividend(dividend);
				trimZeros(trimmedDividend, true);

				// We are treating the dividend and the denominator as integers so we can ignore the floating parts
				if (binaryFloatGreaterThan(trimmedDividend, "0", denominator, "0"))
					divides = true;
				else
					divides = (trimmedDividend == denominator);
			}

			if (divides)
			{
				// After a division update the remainder
				const auto subtractionResult = subtractBinaryIntegers(dividend, denominator);
				remainder = subtractionResult + ((index < remainder.length())
					? remainder.substr(index)
					: string(index + 1 - remainder.length(), '0')); // if the remainder string length is too short then append extra 0s (index is 0 based so add 1)

				if (index <= decimalPosition)
					integerResult.append(One);
				else
					fractionResult.append(One);
			}
			else
			{
				remainder.append(Zero);
				if (index <= decimalPosition)
				{
					// Only add 0s once the first 1 has been added
					if (integerResult.length() > 0)
						integerResult.append(Zero);
				}
				else
				{
					fractionResult.append(Zero);
				}
			}

			++index;
		}

		const auto intResult = (integerResult.length() > 0) ? convertIntegerPartFromBinary(integerResult) : 0.0f;
		const auto fractResult = (fractionResult.length() > 0) ? convertFractionalPartFromBinary(fractionResult) : 0.0f;
		return intResult + fractResult;
	}
}