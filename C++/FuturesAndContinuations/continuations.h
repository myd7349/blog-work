#pragma once

#include <functional>
#include <future>

namespace futures_with_continuations
{
	class IContinuation
	{
	public:
		IContinuation(){}
		virtual ~IContinuation() = default;

		IContinuation(const IContinuation&) = delete;
		IContinuation(IContinuation&&) = delete;
		IContinuation& operator=(const IContinuation&) = delete;
		IContinuation& operator=(IContinuation&&) = delete;

		virtual void execute() = 0;
	};

	template<typename FutureReturnType, typename ContinuationFunction, typename SFINAE = void>
	class Continuation final : public IContinuation
	{
	public:
		Continuation(const ContinuationFunction& continuationFn, const std::shared_future<FutureReturnType>& sharedFuture)
			: IContinuation()
			, m_continuationFn(continuationFn)
			, m_sharedFuture(sharedFuture){}

		~Continuation(){}

		// Disable copying and moving of a Continuation
		Continuation(Continuation&& other) = delete;
		Continuation(const Continuation& other) = delete;
		Continuation& operator=(const Continuation&) = delete;
		Continuation& operator=(Continuation&& other) = delete;

		void execute() override
		{
			m_continuationFn();
		}

	private:
		ContinuationFunction m_continuationFn;
		std::shared_future<FutureReturnType> m_sharedFuture;
	};

	template<typename FutureReturnType, typename ContinuationFunction>
	class Continuation<FutureReturnType, ContinuationFunction, typename std::enable_if<!std::is_void<FutureReturnType>::value>::type> final : public IContinuation
	{
	public:
		Continuation(const ContinuationFunction& continuationFn, const std::shared_future<FutureReturnType>& sharedFuture)
			: IContinuation()
			, m_continuationFn(continuationFn)
			, m_sharedFuture(sharedFuture){}

		~Continuation(){}

		// Disable copying and moving of a Continuation
		Continuation(Continuation&& other) = delete;
		Continuation(const Continuation& other) = delete;
		Continuation& operator=(const Continuation&) = delete;
		Continuation& operator=(Continuation&& other) = delete;

		void execute() override
		{
			m_continuationFn(m_sharedFuture.get());
		}

	private:
		ContinuationFunction m_continuationFn;
		std::shared_future<FutureReturnType> m_sharedFuture;
	};

	// Helper function to create a continuation
	template<typename FutureReturnType, typename ContinuationFunction>
	std::unique_ptr<IContinuation> make_unique_continuation(const ContinuationFunction& continuationFunction, const std::shared_future<FutureReturnType>& sharedFuture)
	{
		return std::unique_ptr<IContinuation>(new Continuation<FutureReturnType, ContinuationFunction>(continuationFunction, sharedFuture));
	}
}