#include "CppUnitTest.h"

#include "futures_and_continuations.h"

#include <concrt.h>
#include <chrono>
#include <future>
#include <thread>
#include <windows.h>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace
{
	const auto sleepDelay = 2;
}

namespace futures_with_continuations
{
	TEST_CLASS(FuturesAndContinuationsTest)
	{
	public:

		TEST_METHOD(FuturesWithContinuations_ForSingleArgumentLambda_ExecutesLogicToProduceTheFuture)
		{
			// Arrange
			auto futureFn = [](const double value){ std::this_thread::sleep_for(std::chrono::seconds(sleepDelay)); return sqrt(value); };
			auto testValue = 4.0;
			auto expected = sqrt(testValue);
			FutureWithContinuations<double> testFuture(futureFn, testValue);

			// Act
			testFuture.wait();
			auto result = testFuture.get();

			// Assert
			Assert::AreEqual(expected, result, L"FutureWithContinuation did produce the expected future");
		}

		TEST_METHOD(FuturesWithContinuations_ForNoArgumentLambda_ExecutesLogicToProduceTheFuture)
		{
			// Arrange
			auto testValue = 4.0;
			auto futureFn = [testValue](){ std::this_thread::sleep_for(std::chrono::seconds(sleepDelay)); return sqrt(testValue); };
			auto expected = sqrt(testValue);
			FutureWithContinuations<double> testFuture(futureFn);

			// Act
			testFuture.wait();
			auto result = testFuture.get();

			// Assert
			Assert::AreEqual(expected, result, L"FutureWithContinuation did produce the expected future");
		}

		TEST_METHOD(FuturesWithContinuations_WhenCreatedWithASingleContinuation_ExecutesTheContinuationAfterTheFuture)
		{
			// Arrange
			auto result = 0.0;
			auto testValue = 4.0;
			auto continuationFn = [&result, testValue](){ result = sqrt(testValue); };
			auto futureFn = [](const double value){ std::this_thread::sleep_for(std::chrono::seconds(sleepDelay)); };
			auto expected = sqrt(testValue);
			FutureWithContinuations<void> testFuture(futureFn, testValue);

			// Act
			testFuture.then(continuationFn);
			testFuture.wait();

			// Assert
			Assert::AreEqual(expected, result, L"Continuation didn't produce the expected square root value for future created with a single continuation");
		}

		TEST_METHOD(FuturesWithContinuations_CallingWaitMultipleTimesOnFuture_DoesntThrow)
		{
			// Arrange
			auto futureFn = [](const double value){ std::this_thread::sleep_for(std::chrono::seconds(sleepDelay)); };
			auto testValue = 4.0;
			FutureWithContinuations<void> testFuture(futureFn, testValue);

			try
			{
				// Act
				testFuture.wait();
				testFuture.wait();
			}
			catch (...)
			{
				// Assert
				Assert::Fail(L"Multiple calls to wait on Future should not have thrown");
			}
		}

		TEST_METHOD(FuturesWithContinuations_CallingGetMultipleTimesOnFuture_DoesntThrow)
		{
			// Arrange
			auto futureFn = [](const double value){ std::this_thread::sleep_for(std::chrono::seconds(sleepDelay)); };
			auto testValue = 4.0;
			FutureWithContinuations<void> testFuture(futureFn, testValue);

			try
			{
				// Act
				testFuture.get();
				testFuture.get();
			}
			catch (...)
			{
				// Assert
				Assert::Fail(L"Multiple calls to wait on Future should not have thrown");
			}
		}

		TEST_METHOD(FuturesWithContinuations_CallingGetOnFuture_RetrievesFutureResult)
		{
			// Arrange
			auto futureFn = [](const double value){ std::this_thread::sleep_for(std::chrono::seconds(sleepDelay)); return sqrt(value); };
			auto testValue = 4.0;
			FutureWithContinuations<double> testFuture(futureFn, testValue);
			auto expected = sqrt(testValue);

			// Act
			auto result = testFuture.get();
			result = testFuture.get();

			// Assert
			Assert::AreEqual(expected, result, L"Calling get on future didn't return expected result");
		}

		TEST_METHOD(FuturesWithContinuations_AddingContinuationToActOnFutureResult_ProducesExpectedOutputFromFutureResult)
		{
			// Arrange
			auto result = 0.0;
			const auto testDouble = 4.0;
			auto continuationFn = [&result](const double future){ result = sqrt(future); };
			auto futureFn = [](const double value){ std::this_thread::sleep_for(std::chrono::seconds(sleepDelay)); return value; };
			const auto expected = sqrt(testDouble);
			FutureWithContinuations<double> testFuture(futureFn, testDouble);

			// Act
			testFuture.then(continuationFn);
			testFuture.wait();

			// Assert
			Assert::AreEqual(expected, result, L"Continuation didn't produce the expected square root value for future created with a single continuation");
		}

		TEST_METHOD(FuturesWithContinuations_AddingMultipleContinuationsToActOnFutureResult_ProducesExpectedOutputFromFutureResult)
		{
			// Arrange
			auto result1 = 0.0;
			auto result2 = 0.0;
			const auto testDouble = 4.0;
			auto continuationFn1 = [&result1](const double future){ result1 = sqrt(future); };
			auto continuationFn2 = [&result2](const double future){ result2 = pow(future, 2.0); };
			auto futureFn = [](const double value){ std::this_thread::sleep_for(std::chrono::seconds(sleepDelay)); return value; };
			const auto expected1 = sqrt(testDouble);
			const auto expected2 = pow(testDouble, 2.0);
			FutureWithContinuations<double> testFuture(futureFn, testDouble);

			// Act
			testFuture.then(continuationFn1);
			testFuture.then(continuationFn2);
			testFuture.wait();

			// Assert
			Assert::AreEqual(expected1, result1, L"Continuation didn't produce the expected square root value for future created multiple continuations");
			Assert::AreEqual(expected2, result2, L"Continuation didn't produce the expected squared value for future created multiple continuations");
		}

		// Futures and Continuations using PPL
		TEST_METHOD(PPLTask_FutureAndContinuationConstructedWithMultipleArguments_ExecutesBothTheFutureAndContinuation)
		{
			// Arrange
			auto result = 0.0;
			auto testValue = 4.0;
			auto expected = sqrt(testValue);

			// Act
			concurrency::task<double> pplTask([testValue](){ std::this_thread::sleep_for(std::chrono::seconds(sleepDelay)); return testValue; });
			auto continuation = pplTask.then([expected](const double value){
				auto result = sqrt(value);

				// Assert
				Assert::AreEqual(expected, result, L"Continuation didn't produce the expected square root value for future created with a single continuation");
			});
			pplTask.wait();
		}

	};
}