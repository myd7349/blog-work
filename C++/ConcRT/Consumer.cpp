#include "PCH.h"
#include "Consumer.h"
#include "RLE.h"
#include <thread>

using namespace Concurrency;
using namespace std;

namespace ConcRT
{
	STLConsumer::STLConsumer(ConcurrentQueue<string>& queue)
		: Thread()
		, m_source(queue)
		, m_decodedStrings()
		, m_timeout(0)
	{}

	STLConsumer::~STLConsumer()
	{
		stop();
	}

	bool STLConsumer::consume(string& consumedData)
	{
		return consume(consumedData, Concurrency::COOPERATIVE_TIMEOUT_INFINITE);
	}

	bool STLConsumer::consume(string& consumedData, unsigned int timeout)
	{
		m_timeout = timeout;

		start();

		// Blocking call - consumer needs to populate the queue with a decoded string 
		consumedData = m_decodedStrings.pop();

		// An empty string indicates that the operation timed out
		if (consumedData.empty())
		{
			join();
			return false;
		}

		return true;
	}

	void STLConsumer::run()
	{
		string stringToDecode;
		string decodedString;

		// Blocking call - producer needs to populate the buffer
		while ((stringToDecode = waitForMessage()) != EndMessage)
		{
			m_decodedStrings.push(RLE::decode<string>(stringToDecode));
		}
		m_decodedStrings.push("");
	}

	void STLConsumer::stop()
	{
		m_source.push(EndMessage);
		join();
	}

	string STLConsumer::waitForMessage()
	{
		string message;
		if (m_timeout == Concurrency::COOPERATIVE_TIMEOUT_INFINITE)
		{
			message = m_source.pop();
		}
		else
		{
			if (!m_source.try_pop(message, m_timeout))
			{
				message = EndMessage;
			}
		}

		return message;
	}

	// -----------------------------------------------------------------------
	// -----------------------------------------------------------------------
	// -----------------------------------------------------------------------

	AALConsumer::AALConsumer(ISource<string>& sourceBuffer)
		: agent()
		, m_sourceBuffer(sourceBuffer)
		, m_decodedStrings()
		, m_timeout(0)
	{}

	AALConsumer::~AALConsumer()
	{
		stop();
	}

	bool AALConsumer::consume(string& consumedData)
	{
		return consume(consumedData, Concurrency::COOPERATIVE_TIMEOUT_INFINITE);
	}

	bool AALConsumer::consume(string& consumedData, unsigned int timeout)
	{
		m_timeout = timeout;

		// The consumer only needs to start in the first call to 'consume'
		if (agent_status::agent_created == status())
		{
			start();
		}

		if (agent_status::agent_done != status() && agent_status::agent_canceled != status())
		{
			// Blocking call - consumer needs to populate the queue with a decoded string 
			while (!m_decodedStrings.try_pop(consumedData));

			// An empty string indicates that the operation timed out
			return !consumedData.empty();
		}

		return false;
	}

	void AALConsumer::stop()
	{
		// Blocking call - wait until the consumer is done
		if (agent_status::agent_runnable == status() || agent_status::agent_started == status())
		{
			agent::wait(this);
		}
	}

	void AALConsumer::run()
	{
		string stringToDecode;
		string decodedString;

		try
		{
			// Blocking call - producer needs to populate the buffer before the timeout expires
			while ((stringToDecode = receive(m_sourceBuffer, m_timeout)) != EndMessage)
			{
				decodedString = RLE::decode<string>(stringToDecode);
				m_decodedStrings.push(decodedString);
			}
		}
		catch (const operation_timed_out&)
		{
			m_decodedStrings.push("");
		}
		done();
	}
}