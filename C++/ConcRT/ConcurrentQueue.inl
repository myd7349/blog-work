namespace ConcRT
{
	template <class T, class ContainerClass>
	ConcurrentQueue<T, ContainerClass>::ConcurrentQueue()
		: m_mutex()
		, m_items(std::queue<T, ContainerClass>())
		, m_cv()
	{}

	template <class T, class ContainerClass>
	ConcurrentQueue<T, ContainerClass>::ConcurrentQueue(const std::queue<T, ContainerClass>& items)
		: m_mutex()
		, m_items(items)
		, m_cv()
	{}

	template <class T, class ContainerClass>
	void ConcurrentQueue<T, ContainerClass>::push(const T& item)
	{
		std::unique_lock<std::mutex> lock(m_mutex);
		m_items.push(item);
		lock.unlock();
		m_cv.notify_all();
	}

	template <class T, class ContainerClass>
	bool ConcurrentQueue<T, ContainerClass>::try_pop(T& item)
	{
		std::unique_lock<std::mutex> scopedLock(m_mutex);
		if (!m_items.empty())
		{
			return popInternal(item);
		}

		return false;
	}

	template <class T, class ContainerClass>
	bool ConcurrentQueue<T, ContainerClass>::try_pop(T& item, std::size_t timeout)
	{
		std::unique_lock<std::mutex> scopedLock(m_mutex);
		while (m_items.empty())
		{
			std::chrono::milliseconds ms(timeout);
			auto now = std::chrono::system_clock::now();
			if (m_cv.wait_until(scopedLock, now + ms) == cv_status::timeout)
			{
				return false;
			}
		}

		return popInternal(item);
	}

	template <class T, class ContainerClass>
	T ConcurrentQueue<T, ContainerClass>::pop()
	{
		std::unique_lock<std::mutex> scopedLock(m_mutex);
		while (m_items.empty())
		{
			m_cv.wait(scopedLock);
		}

		T item;
		popInternal(item);
		return item;
	}

	template <class T, class ContainerClass>
	bool ConcurrentQueue<T, ContainerClass>::empty()
	{
		std::lock_guard<std::mutex> scopedLock(m_mutex);
		return m_items.empty();
	}

	template <class T, class ContainerClass>
	std::size_t ConcurrentQueue<T, ContainerClass>::size()
	{
		std::lock_guard<std::mutex> scopedLock(m_mutex);
		return m_items.size();
	}

	template <class T, class ContainerClass>
	std::mutex& ConcurrentQueue<T, ContainerClass>::getMutex()
	{
		return m_mutex;
	}

	template <class T, class ContainerClass>
	bool ConcurrentQueue<T, ContainerClass>::popInternal(T& item)
	{
		item = m_items.front();
		m_items.pop();
		return true;
	}
}