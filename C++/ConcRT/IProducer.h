#pragma once

namespace ConcRT
{
	class __declspec(dllexport) IProducer
	{
	public:
		virtual ~IProducer(){}
		virtual void produce(const std::string& stringToEncode) = 0;
		virtual void stop() = 0;
	};
}