#pragma once

namespace ConcRT
{
	namespace RLE
	{	
		// Algorithm for encoding:
		//	A positive byte N followed by a byte B indicates a run of N copies of B
		//	A negative byte -N followed by N bytes indicates that those bytes should be copied directly

		template<typename T, typename Iter>
		T encode(Iter start, Iter end);

		template<typename T>
		T encode(T input);

		template<typename T, typename Iter>
		T decode(Iter start, Iter end);

		template<typename T>
		T decode(T input);
	}
}

#include "RLE.inl"