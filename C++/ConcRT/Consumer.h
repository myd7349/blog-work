#pragma once

#include <agents.h>
#include "ConcurrentQueue.h"
#include <concurrent_queue.h>
#include "IConsumer.h"
#include "Thread.h"

namespace ConcRT
{
	class __declspec(dllexport) STLConsumer final : public Thread, virtual public IConsumer
	{
	public:
		explicit
		STLConsumer(ConcurrentQueue<std::string>& queue);
		~STLConsumer();

		// Blocks until data is available
		bool consume(std::string& consumedData) override;
		// Blocks until data is available or action times out
		bool consume(std::string& consumedData, unsigned int timeout) override;
		void stop() override;

	protected:
		void run() override;

	private:
		ConcurrentQueue<std::string>& m_source;
		ConcurrentQueue<std::string> m_decodedStrings;
		unsigned int m_timeout;

		STLConsumer(STLConsumer&) = delete;
		STLConsumer(STLConsumer&&) = delete;
		STLConsumer& operator=(STLConsumer&) = delete;
		STLConsumer& operator=(STLConsumer&&) = delete;

		std::string STLConsumer::waitForMessage();
	};

	// -----------------------------------------------------------------------
	// -----------------------------------------------------------------------
	// -----------------------------------------------------------------------

	class __declspec(dllexport) AALConsumer final : public Concurrency::agent, virtual public IConsumer
	{
	public:
		explicit
		AALConsumer(Concurrency::ISource<std::string>& sourceBuffer);
		~AALConsumer();

		// Blocks until data is available
		bool consume(std::string& consumedData) override;
		// Blocks until data is available or action times out
		bool consume(std::string& consumedData, unsigned int timeout) override;
		void stop() override;

	protected:
		void run() override;

	private:
		Concurrency::ISource<std::string>& m_sourceBuffer;
		Concurrency::concurrent_queue<std::string> m_decodedStrings;
		unsigned int m_timeout;

		AALConsumer(AALConsumer&) = delete;
		AALConsumer(AALConsumer&&) = delete;
		AALConsumer& operator=(AALConsumer&) = delete;
		AALConsumer& operator=(AALConsumer&&) = delete;
	};
}