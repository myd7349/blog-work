#include "RLE.h"

#include <algorithm>
#include <deque>
#include <sstream>

using namespace std;

const size_t MaxCharacterCount = 127; // byte range -127 -> 127

namespace ConcRT
{
	namespace RLE
	{
		template<typename T, typename Iter>
		void clearSingleCharTracking(stringstream& singleChars, int& singleCharCount)
		{
			singleCharCount = 0;
			singleChars.str(string());
			singleChars.clear();
		}

		// Algorithm for encoding:
		//	A positive byte N followed by a byte B indicates a run of N copies of B ... max copies of B == 127
		//	A negative byte -N followed by N bytes indicates that those bytes should be copied directly ... max single chars == 127
		template<typename T, typename Iter>
		T encode(Iter start, Iter end)
		{
			stringstream encodedSS;
			stringstream singleChars;
			int singleCharCount = 0;
			auto first = start;

			while(first < end)
			{
				auto run = std::find_if(first, end, [=](char c){ return c != *first; });
				size_t runCount = (run - first);
				char character = *first;

				if(runCount > 1)
				{	
					// Store the single characters as well as their quantity (-N followed by N bytes) before any runs 
					if(singleCharCount > 0)
					{
						encodedSS << static_cast<char>(singleCharCount * -1) << singleChars.str();

						clearSingleCharTracking<T, Iter>(singleChars, singleCharCount);
					}

					// Store the run (N copies of B)
					if(runCount > MaxCharacterCount)
					{
						const size_t runsOfMaxCount = runCount / MaxCharacterCount;
						const size_t remainder = runCount % MaxCharacterCount;

						for(size_t i = 0; i < runsOfMaxCount; ++i)
						{
							encodedSS << static_cast<char>(MaxCharacterCount) << character;
						}

						if(remainder > 0)
						{
							encodedSS << static_cast<char>(remainder) << character;
						}
					}
					else
					{
						encodedSS << static_cast<char>(runCount) << character;
					}
				}
				else
				{
					++singleCharCount;
					singleChars << character;

					// If we reach the last of the input or if we have reached the maximum number of characters
					// print the group of single characters
					if(run == end || singleCharCount == MaxCharacterCount )
					{
						encodedSS << static_cast<char>(singleCharCount * -1) << singleChars.str();

						clearSingleCharTracking<T, Iter>(singleChars, singleCharCount);
					}
				}

				// Update the iterator
				first = run;
			}

			string encodedString(encodedSS.str());
			return T(encodedString.cbegin(), encodedString.cend());
		}

		template<typename T>
		T encode(T input)
		{
			return encode<T>(input.cbegin(), input.cend());
		}

		template<typename T, typename Iter>
		T decode(Iter start, Iter end)
		{
			// Store count byte and advance iterator
			auto currentCountByte = start++;
			auto charsToDecode = static_cast<size_t>(abs(static_cast<int>(*currentCountByte)));
			stringstream decodedSS;
			auto currentCountByteValue = static_cast<int>(*currentCountByte);

			auto first = start;
			bool firstIteration = true;

			while(first < end)
			{
				// Update counters on subsequent iterations
				if(!firstIteration)
				{
					currentCountByte = first;
					currentCountByteValue = static_cast<int>(*currentCountByte);
					charsToDecode = static_cast<size_t>(abs(currentCountByteValue));
					start = first;
				}

				if(currentCountByteValue < 0)
				{
					// Move beyond the count byte
					const auto firstSingleChar = (firstIteration) ? first : ++first;

					// Move the iterator to the last of the range
					const auto lastSingleChar = (first += charsToDecode);
					charsToDecode = 0;

					decodedSS << string(firstSingleChar, lastSingleChar);
				}
				else
				{
					decodedSS << string(charsToDecode, (firstIteration) ? *first : *++first);
					charsToDecode = 0;

					// Move to next group
					++first;
				}

				firstIteration = false;
			}

			string decodedString(decodedSS.str());
			return T(decodedString.cbegin(), decodedString.cend());
		}

		template<typename T>
		T decode(T input)
		{
			return decode<T>(input.cbegin(), input.cend());
		}
	}
}