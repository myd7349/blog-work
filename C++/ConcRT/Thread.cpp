#include "PCH.h"
#include "Thread.h"

using namespace std;

namespace ConcRT
{
	Thread::Thread()
		: m_thread()
		, m_canStart(true)
	{}

	Thread::~Thread()
	{}

	bool Thread::start()
	{
		if (m_canStart)
		{
			m_thread.reset(new thread([&](){ run(); }));
			m_canStart = false;

			return true;
		}

		return false;
	}

	bool Thread::join()
	{
		if (!m_canStart)
		{
			m_thread->join();
			m_canStart = true;
			return true;
		}

		return false;
	}
}