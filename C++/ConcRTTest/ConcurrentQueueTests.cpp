#include <ConcurrentQueue.h>
#include "CppUnitTest.h"
#include <memory>
#include <thread>
#include <windows.h>

using namespace ConcRT;
using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace std;
using namespace std::chrono;
using namespace std::this_thread;

namespace ConcRT_Test
{
	// Container to use for the queue in the ConcurrentQueue
	// The various methods are wrapped in order to provide a delay so that the 
	// mutex can be queried in the tests to ensure that the queue is synchronized
	template <typename T>
	class TestQueue final
	{
	public:
		typedef typename queue<T>::value_type value_type;
		typedef typename queue<T>::size_type size_type;
		typedef typename queue<T>::reference reference;
		typedef typename queue<T>::const_reference const_reference;

		TestQueue() : m_queue(), m_delay(10){}

		explicit
		TestQueue(const int delay) : m_queue(), m_delay(delay){}

		void push_back(const T& item) { m_queue.push(item); sleep_for(milliseconds(m_delay)); }
		T& front() { return m_queue.front(); }
		T& back() { return m_queue.back(); }
		void pop_front() { m_queue.pop(); sleep_for(milliseconds(m_delay)); }
		bool empty() const { sleep_for(milliseconds(m_delay)); return m_queue.empty(); }
		size_t size() const { sleep_for(milliseconds(m_delay)); return m_queue.size(); }

	private:
		queue<T> m_queue;
		int m_delay;
	};


	TEST_CLASS(ConcRT_ConcurrentQueueTests)
	{
	public:
		
		TEST_METHOD(Empty_ForEmptyQueue_ReturnsTrue)
		{
			// Arrange:
			ConcurrentQueue<int> concurrentQueue;

			// Act:
			auto result = concurrentQueue.empty();

			// Assert:
			Assert::IsTrue(result, L"Expected queue to be empty as nothing was added to it");
		}

		TEST_METHOD(Empty_ForNonEmptyQueue_ReturnsFalse)
		{
			// Arrange:
			ConcurrentQueue<int> concurrentQueue;
			concurrentQueue.push(10);

			// Act:
			auto result = concurrentQueue.empty();

			// Assert:
			Assert::IsFalse(result, L"Expected queue to contain at least 1 item");
		}

		TEST_METHOD(Empty_ForQueryFromOtherThread_LocksTheMutex)
		{
			// Arrange:
			ConcurrentQueue<int, TestQueue<int>> concurrentQueue;
			concurrentQueue.push(10);
			auto& mutex = concurrentQueue.getMutex();

			// Act:
			thread newThread([&concurrentQueue]()
			{
				concurrentQueue.empty();
			});
			bool result = mutex.try_lock();
			newThread.join();

			// Assert:
			Assert::IsFalse(result, L"Expected mutex to be locked during empty query");
		}

		TEST_METHOD(Size_ForQueryFromOtherThread_LocksTheMutex)
		{
			// Arrange:
			ConcurrentQueue<int, TestQueue<int>> concurrentQueue;
			concurrentQueue.push(10);
			auto& mutex = concurrentQueue.getMutex();

			// Act:
			thread newThread([&concurrentQueue]()
			{
				concurrentQueue.size();
			});
			bool result = mutex.try_lock();
			newThread.join();

			// Assert:
			Assert::IsFalse(result, L"Expected mutex to be locked during size query");
		}

		TEST_METHOD(Push_AddingMultipleItems_IncreasesQueueSize)
		{
			// Arrange:
			ConcurrentQueue<int, TestQueue<int>> concurrentQueue;
			const size_t expected = 9;

			// Act:
			for (size_t i = 0; i < expected; ++i)
				concurrentQueue.push(rand() % 100);

			// Assert:
			const auto result = concurrentQueue.size();
			Assert::AreEqual(expected, result, L"Size of queue is incorrect after adding 9 items");
		}

		TEST_METHOD(Push_AddingItemOnAnotherThread_LocksTheMutex)
		{
			// Arrange:
			queue<int, TestQueue<int>> testQueue;
			ConcurrentQueue<int, TestQueue<int>> concurrentQueue(testQueue);
			auto& mutex = concurrentQueue.getMutex();

			// Act:
			thread newThread([&concurrentQueue]()
			{
				concurrentQueue.push(10);
			});			
			bool result = mutex.try_lock();
			newThread.join();

			// Assert:
			Assert::IsFalse(result, L"Expected mutex to be locked during push command");
		}

		TEST_METHOD(TryPop_RemovingMultipleItems_DecreasesQueueSize)
		{
			// Arrange:
			ConcurrentQueue<int, TestQueue<int>> concurrentQueue;
			const size_t expected = 5;

			for (size_t i = 0; i < expected * 2; ++i)
				concurrentQueue.push(rand() % 100);

			// Act:
			for (size_t i = 0; i < expected; ++i)
			{
				int value;
				concurrentQueue.try_pop(value);
			}

			// Assert:
			const auto result = concurrentQueue.size();
			Assert::AreEqual(expected, result, L"Size of queue is incorrect after removing 5 items");
		}

		TEST_METHOD(TryPop_RemovingItemOnAnotherThread_LocksTheMutex)
		{
			// Arrange:
			queue<int, TestQueue<int>> testQueue;
			ConcurrentQueue<int, TestQueue<int>> concurrentQueue(testQueue);
			auto& mutex = concurrentQueue.getMutex();
			bool valuePopped = false;
			concurrentQueue.push(4);

			// Act:
			thread newThread([&concurrentQueue, &valuePopped]()
			{
				int value;
				valuePopped = concurrentQueue.try_pop(value);
			});
			bool result = mutex.try_lock();
			newThread.join();

			// Assert:
			Assert::IsTrue(valuePopped, L"Pop command failed on a separate thread");
			Assert::IsFalse(result, L"Expected mutex to be locked during pop command");
		}

		TEST_METHOD(TryPop_RemovingItemFromEmptyQueueWithTimeoutSpecified_ReturnsFalseAfterTimeout)
		{
			// Arrange:
			queue<int, TestQueue<int>> testQueue;
			ConcurrentQueue<int, TestQueue<int>> concurrentQueue(testQueue);
			int item = 0;

			// Act:
			auto result = concurrentQueue.try_pop(item, 10);

			// Assert:
			Assert::IsFalse(result, L"try_pop should have returns false after a timeout");
		}

		TEST_METHOD(TryPop_RemovingItemWithTimeoutSpecified_PopsLastValuePushedOntoQueue)
		{
			// Arrange:
			queue<int, TestQueue<int>> testQueue;
			ConcurrentQueue<int, TestQueue<int>> concurrentQueue(testQueue);
			int result = 0;
			int expected = 7;

			// Act:
			concurrentQueue.push(expected);
			auto valuePopped = concurrentQueue.try_pop(result, 10);

			// Assert:
			Assert::IsTrue(valuePopped, L"try_pop should have returns true after popping the top value off the queue");
			Assert::AreEqual(expected, result, L"Value popped off the queue didn't match the expected value");
		}

		TEST_METHOD(Pop_RemovingItemOnAnotherThread_BlocksUntilQueueIsPopulated)
		{
			// Arrange:
			queue<int, TestQueue<int>> testQueue;
			ConcurrentQueue<int, TestQueue<int>> concurrentQueue(testQueue);
			int value = 0;
			bool valuePopped = false;

			// Act:
			thread newThread([&]()
			{
				value = concurrentQueue.pop();
				valuePopped = true;
			});

			// Assert:
			Assert::IsFalse(valuePopped, L"Value shouldn't have been popped from an empty queue");

			concurrentQueue.push(7);
			newThread.join();
		}

		TEST_METHOD(Pop_RemovingItemOnAnotherThread_PopsLastValuePushedOntoQueue)
		{
			// Arrange:
			queue<int, TestQueue<int>> testQueue;
			ConcurrentQueue<int, TestQueue<int>> concurrentQueue(testQueue);
			int result = 0;
			int expected = 7;

			// Act:
			thread newThread([&]()
			{
				result = concurrentQueue.pop();
			});
			concurrentQueue.push(expected);
			newThread.join();

			// Assert:
			Assert::AreEqual(expected, result, L"Value popped off the queue didn't match the expected value");
		}

		TEST_METHOD(Pop_RemovingItemFromEmptyQueue_DoesntReturnIfQueueIsntPopulated)
		{
			// Arrange:
			queue<int, TestQueue<int>> testQueue;
			ConcurrentQueue<int, TestQueue<int>> concurrentQueue(testQueue);
			int result = 0;

			// Act:
			thread newThread([&]()
			{
				result = concurrentQueue.pop();
			});

			// Assert:
			Assert::IsTrue(newThread.joinable(), L"Thread should still be executing waiting for the queue to be populated");

			concurrentQueue.push(1);
			newThread.join();
		}
	};
}