#include <ConcurrentQueue.h>
#include <Consumer.h>
#include "CppUnitTest.h"
#include <Producer.h>
#include <RLE.h>
#include <vector>
#include <thread>

using namespace ConcRT;
using namespace Concurrency;
using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace std;

namespace ConcRT_Test
{
	const string StringToEncode = "11122234566677777";

	const wchar_t* assert_message(const stringstream& message)
	{
		const auto messageStr = message.str();
		const auto wMessage = wstring(messageStr.begin(), messageStr.end());
		return wMessage.c_str();
	}

	TEST_CLASS(ConcRT_STLProducerConsumerTests)
	{
	public:
		
		TEST_METHOD(Produce_ForStandardString_AddsRLEEncodedString)
		{
			// Arrange:
			ConcurrentQueue<string> queue;
			STLProducer producer(queue);
			const auto expected = RLE::encode(StringToEncode);

			// Act:
			producer.produce(StringToEncode);
			producer.stop();

			// Assert:
			Assert::IsFalse(queue.empty(), L"Queue shouldn't be empty");
			Assert::AreEqual(expected, queue.pop(), L"Queue doesn't contain the expected encoded string");
		}

		TEST_METHOD(Produce_ForEmptyString_DoesNothing)
		{
			// Arrange:
			ConcurrentQueue<string> queue;
			STLProducer producer(queue);
			const string emptyString = "";

			// Act:
			producer.produce(emptyString);
			producer.stop();

			// Assert:
			Assert::IsTrue(queue.empty(), L"Queue should be empty");
		}

		TEST_METHOD(ProducerStop_CalledBeforeProduce_HasNoEffectOnProducer)
		{
			// Arrange:
			ConcurrentQueue<string> queue;
			STLProducer producer(queue);

			// Act:
			producer.stop();

			// Assert:
			// There is no check to make sure calling stop doesn't throw an exception
		}

		TEST_METHOD(ProducerStop_CalledMultiplteTimes_HasNoEffectOnProducer)
		{
			// Arrange:
			ConcurrentQueue<string> queue;
			STLProducer producer(queue);

			// Act:
			producer.produce(StringToEncode);
			producer.stop();
			producer.stop();

			// Assert:
			// There is no check to make sure calling stop doesn't throw an exception
		}

		TEST_METHOD(Consume_ForEmptyQueue_Blocks)
		{
			// Arrange:
			ConcurrentQueue<string> queue;
			STLConsumer consumer(queue);
			string result;

			// Act:
			thread newThread([&]()
			{
				consumer.consume(result);
			});

			// Assert:
			Assert::IsTrue(newThread.joinable(), L"Consumer should have blocked on the thread");

			queue.push(RLE::encode(string("123")));
			newThread.join();
			consumer.stop();
		}

		TEST_METHOD(Consume_ForNonEmptyQueue_DecodesExpectedString)
		{
			// Arrange:
			ConcurrentQueue<string> queue;
			STLConsumer consumer(queue);
			const string expected = StringToEncode;
			queue.push(RLE::encode(expected));
			string result;

			// Act:
			consumer.consume(result);
			consumer.stop();

			// Assert:
			stringstream message;
			message << "The Consumer produced " << result << " when we were expecting " << expected << endl;
			Assert::AreEqual(expected, result, assert_message(message));
		}

		TEST_METHOD(ConsumerStop_CalledBeforeConsume_HasNoEffectOnConsumer)
		{
			// Arrange:
			ConcurrentQueue<string> queue;
			STLConsumer consumer(queue);

			// Act:
			consumer.stop();

			// Assert:
			Assert::AreEqual(string("End"), queue.pop(), L"Queue should be contain 'End'");
		}

		TEST_METHOD(Consume_ForAnEmptyQueueAndATimeoutSpecified_TimesOutAndReturnsFalse)
		{
			// Arrange:
			ConcurrentQueue<string> queue;
			STLConsumer consumer(queue);
			string consumedData;

			// Act:
			const auto result = consumer.consume(consumedData, 10);
			consumer.stop();

			// Assert:
			Assert::IsFalse(result, L"consume should return false after timing out");
		}

		TEST_METHOD(ConsumerStop_CalledMultipleTimes_HasNoEffectOnConsumer)
		{
			// Arrange:
			ConcurrentQueue<string> queue;
			STLProducer producer(queue);
			STLConsumer consumer(queue);
			string consumedData;
			
			producer.produce(StringToEncode);
			consumer.consume(consumedData);
			producer.stop();			

			// Act:
			consumer.stop();
			consumer.stop();

			// Assert:
			Assert::AreEqual(string("End"), queue.pop(), L"Queue should be contain 'End'");
		}
	};

	// -----------------------------------------------------------------------
	// -----------------------------------------------------------------------
	// -----------------------------------------------------------------------

	TEST_CLASS(ConcRT_AALProducerConsumerTests)
	{
	public:

		TEST_METHOD(Produce_ForStandardString_AddsRLEEncodedString)
		{
			// Arrange:
			const auto expected = RLE::encode(StringToEncode);
			unbounded_buffer<string> buffer;
			AALProducer producer(buffer);

			// Act:
			producer.produce(StringToEncode);
			const auto result = receive(buffer);
			producer.stop();

			// Assert:
			Assert::AreEqual(expected, result, L"Producer didn't produce the expected encoded string");
		}

		TEST_METHOD(Produce_CalledForFirstTime_ChangesAgentStatusFromCreated)
		{
			// Arrange:
			unbounded_buffer<string> buffer;
			AALProducer producer(buffer);
			const auto statusBefore = producer.status();

			// Act:
			producer.produce(StringToEncode);
			receive(buffer);
			const auto statusAfter = producer.status();
			producer.stop();

			// Assert:
			Assert::IsTrue(agent_status::agent_created == statusBefore, L"Producer should be in 'created' state after construction");
			Assert::IsTrue(agent_status::agent_created != statusAfter, L"Producer should not be in 'created' state after first call to Produce");
		}

		TEST_METHOD(Produce_CalledMultipleTimes_ProducesMultipleItems)
		{
			// Arrange:
			unbounded_buffer<string> buffer;
			AALProducer producer(buffer);
			const size_t numIterations = 3;
			vector<string> result;

			// Act:
			for (size_t i = 0; i < numIterations; ++i)
				producer.produce(StringToEncode);
			for (size_t i = 0; i < numIterations; ++i)
				result.push_back(receive(buffer));
			producer.stop();
			
			// Assert:
			stringstream message;
			message << "The Producer produced " << result.size() << " items when it was expected to produce " << numIterations << endl;
			Assert::AreEqual(numIterations, result.size(), assert_message(message));
		}

		TEST_METHOD(ProducerStop_CalledBeforeProduce_HasNoEffectOnAgentStatus)
		{
			// Arrange:
			unbounded_buffer<string> buffer;
			AALProducer producer(buffer);

			// Act:
			producer.stop();

			// Assert:
			Assert::IsTrue(agent_status::agent_created == producer.status(), L"Producer should be in 'created' state after attempting to stop before producing");
		}

		TEST_METHOD(ProducerStop_ForActiveProducer_MovesProducerToDoneState)
		{
			// Arrange:
			unbounded_buffer<string> buffer;
			AALProducer producer(buffer);

			// Act:
			producer.produce(StringToEncode);
			producer.stop();

			// Assert:
			Assert::IsTrue(agent_status::agent_done == producer.status(), L"Producer should be in 'done' state after being stopped");
		}

		TEST_METHOD(Produce_CalledAfterStop_HasNoEffectOnAgentStatus)
		{
			// Arrange:
			unbounded_buffer<string> buffer;
			AALProducer producer(buffer);

			// Act:
			producer.produce(StringToEncode);
			producer.stop();
			producer.produce(StringToEncode);

			// Assert:
			Assert::IsTrue(agent_status::agent_done == producer.status(), L"produce should have no effect on Producer agent status after being stopped");
		}

		TEST_METHOD(ProducerStop_CalledMultiplteTimes_HasNoEffectOnAgentStatus)
		{
			// Arrange:
			unbounded_buffer<string> buffer;
			AALProducer producer(buffer);

			// Act:
			producer.produce(StringToEncode);
			producer.stop();
			producer.stop();

			// Assert:
			Assert::IsTrue(agent_status::agent_done == producer.status(), L"Producer should be in 'done' state after attempting to stop twice");
		}

		TEST_METHOD(Consumer_ForEmptyBuffer_Blocks)
		{
			// Arrange:
			unbounded_buffer<string> buffer;
			AALConsumer consumer(buffer);
			string consumedData;

			// Act:
			thread newThread([&]()
			{
				consumer.consume(consumedData);
			});

			// Assert:
			Assert::IsTrue(newThread.joinable(), L"Consumer should have blocked on the thread");

			send<string>(buffer, "Test");
			send<string>(buffer, "End");
			consumer.stop();
			newThread.join();
		}

		TEST_METHOD(Consume_ForNonEmptyQueue_DecodesExpectedString)
		{
			// Arrange:
			unbounded_buffer<string> buffer;
			AALProducer producer(buffer);
			AALConsumer consumer(buffer);
			const string expected = StringToEncode;
			string result;

			producer.produce(StringToEncode);
			producer.stop();

			// Act:
			consumer.consume(result);
			consumer.stop();

			// Assert:
			stringstream message;
			message << "The Consumer should have produced " << expected << " but instead it produced " << result << endl;
			Assert::AreEqual(expected, result, assert_message(message));
		}

		TEST_METHOD(Consume_AfterBeingStopped_ReturnsFalse)
		{
			// Arrange:
			unbounded_buffer<string> buffer;
			AALProducer producer(buffer);
			AALConsumer consumer(buffer);
			string consumedData;

			producer.produce(StringToEncode);
			consumer.consume(consumedData);
			producer.stop();
			consumer.stop();

			// Act:
			const auto result = consumer.consume(consumedData);

			// Assert:
			Assert::IsFalse(result, L"consume should return false after stop has been called");
		}

		TEST_METHOD(Consume_ForAnEmptyQueueAndATimeoutSpecified_TimesOutAndReturnsFalse)
		{
			// Arrange:
			unbounded_buffer<string> buffer;
			AALConsumer consumer(buffer);
			string consumedData;

			// Act:
			const auto result = consumer.consume(consumedData, 10);
			consumer.stop();

			// Assert:
			Assert::IsFalse(result, L"consume should return false after timing out");
		}

		TEST_METHOD(ConsumerStop_CalledMultipleTimes_HasNoEffectOnAgentStatus)
		{
			// Arrange:
			unbounded_buffer<string> buffer;
			AALProducer producer(buffer);
			AALConsumer consumer(buffer);
			string consumedData;

			producer.produce(StringToEncode);
			consumer.consume(consumedData);
			producer.stop();
			consumer.stop();
			consumer.stop();

			// Act:
			const auto result = consumer.consume(consumedData);

			// Assert:
			Assert::IsTrue(agent_status::agent_done == consumer.status(), L"stop called twice should have no effect on Consumer agent status");
		}
	};
}