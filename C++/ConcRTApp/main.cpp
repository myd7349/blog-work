#include "ConcurrentQueue.h"
#include "Consumer.h"
#include "IConsumer.h"
#include "IProducer.h"
#include <iostream>
#include "Producer.h"
#include <sstream>
#include <thread>
#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
#include <windows.h>

using namespace ConcRT;
using namespace Concurrency;
using namespace std;

void renderInstructions()
{
	// Clear the screen
	system("cls");

	cout << "Producer Consumer Instructions:" << endl;
	cout << "* Press A to start an AAL Producer Consumer Pair" << endl;
	cout << "* Press S to start an STL Producer Consumer Pair" << endl;
	cout << "Once a Producer Consumer Pair has been started" << endl;
	cout << "* Press P to produce a string to encode" << endl;
	cout << "* Press C to consume any string produced by the Producer (times out after 5 seconds)" << endl;
	cout << "Press Esc to exit" << endl;
}

string generateRandomString()
{
	stringstream randomNumbers;
	for (size_t i = 0; i < 20; ++i)
	{
		randomNumbers << static_cast<char>('0' + (rand() % 10));
	}
	return randomNumbers.str();
}

int main(int argc, char *argv[])
{
	renderInstructions();

	unique_ptr<unbounded_buffer<string>> buffer;
	unique_ptr<ConcurrentQueue<string>> queue;
	unique_ptr<IProducer> producer;
	unique_ptr<IConsumer> consumer;
	const size_t fiveSeconds = 5000;

	do{
		if (0 != GetAsyncKeyState('A') || 0 != GetAsyncKeyState('a'))
		{
			// Stop any executing Producer & Consumer before destroying the buffer & queue
			if (producer && consumer)
			{
				producer->stop();
				consumer->stop();
			}
			buffer.reset(new unbounded_buffer<string>());
			producer.reset(new AALProducer(*buffer));
			consumer.reset(new AALConsumer(*buffer));
			cout << "AAL Producer & Consumer created." << endl;
		}
		else if (0 != GetAsyncKeyState('S') || 0 != GetAsyncKeyState('s'))
		{
			// Stop any executing Producer & Consumer before destroying the buffer & queue
			if (producer && consumer)
			{
				producer->stop();
				consumer->stop();
			}
			queue.reset(new ConcurrentQueue<string>());
			producer.reset(new STLProducer(*queue));
			consumer.reset(new STLConsumer(*queue));
			cout << "STL Producer & Consumer created." << endl;
		}
		else if (0 != GetAsyncKeyState('P') || 0 != GetAsyncKeyState('p'))
		{
			if (producer && consumer)
			{
				const auto stringToEncode = generateRandomString();
				producer->produce(stringToEncode);
				cout << "Producer encoding: " << stringToEncode.c_str() << endl;
			}
			else
			{
				cout << "You need to create both a Producer and Consumer before proceeding to Produce!" << endl;
			}
		}
		else if (0 != GetAsyncKeyState('C') || 0 != GetAsyncKeyState('c'))
		{
			if (producer && consumer)
			{
				string decodedString;
				if (consumer->consume(decodedString, fiveSeconds))
				{
					cout << "Consumer decoding: " << decodedString.c_str() << endl;
				}
				else
				{
					cout << "Consumer decoding timed out waiting for the Producer to populate the buffer!" << endl << "You will need to re-create the AALProducer & AALConsumer, but you can just produce an item for the STLProducer & STLConsumer and they will continue as normal" << endl;
				}
			}
			else
			{
				cout << "You need to create both a Producer and Consumer before proceeding to Produce!" << endl;
			}
		}

		// Sleep to prevent successive key presses being detected
		Sleep(350);
	} while (0 == GetAsyncKeyState(VK_ESCAPE));

	// Ensure that the Producer and Consumer get destructed before the buffer & queue
	producer.reset();
	consumer.reset();
	buffer.reset();
	queue.reset();

	return 0;
}